import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'OMEGAlpes-web-front-end';
  private screen = 'Board';
  constructor() {
    localStorage.setItem('model', '');
  }

  setBoard() {
    this.screen = 'Board';
  }

  setResult() {
    this.screen = 'Result';
  }

  setCodeEditor() {
    this.screen = 'CodeEditor';
  }

  setSettings() {
    this.screen = 'Settings';
  }

  getBoard() {
    return this.screen === 'Board';
  }

  getResult() {
    return this.screen === 'Result';
  }

  getCodeEditor() {
    return this.screen === 'CodeEditor';
  }

  getSettings() {
    return this.screen === 'Settings';
  }
}
