import { Injectable } from '@angular/core';
import 'snapsvg-cjs';
import SNAPSVG_TYPE, { Paper } from 'snapsvg';
import * as config from '../graphics.config';
import { Subtype } from 'src/app/models/Subtype';

@Injectable({
  providedIn: 'root'
})
export class IconsService {
  private canvas: Paper;

  public getIcon(canvas: Paper, subtype: Subtype): SNAPSVG_TYPE.Element {
    this.canvas = canvas;
    let icon: SNAPSVG_TYPE.Element;
    if (subtype === Subtype.UNSPECIFIED) {
      icon = this.getUnspecifiedIcon();
    } else if (subtype === Subtype.FIXED) {
      icon = this.getFixedIcon();
    } else if (subtype === Subtype.VARIABLE) {
      icon = this.getVariableIcon();
    } else if (subtype === Subtype.STORAGE) {
      icon = this.getStorageIcon();
    } else if (subtype === Subtype.REVERSIBLE) {
      icon = this.getReversibleIcon();
    }
    icon.attr({
      class: 'icon'
    });

    return icon;
  }

    private getReversibleIcon(): SNAPSVG_TYPE.Element {
      const group = this.canvas.g().attr({
        width: config.ICON_WIDTH + 2 * config.PADDING,
        height: config.ICON_HEIGHT + 2 * config.PADDING
      });

      const bkg = this.canvas
        .rect(
          0,
          0,
          config.ICON_WIDTH + 2 * config.PADDING,
          config.ICON_HEIGHT + 2 * config.PADDING,
          config.RECT_RADIUS,
          config.RECT_RADIUS
        )
        .attr({
          fill: 'white'
        });

      const triangle = this.canvas
        .polygon([12, 6, 22, 16, 12, 26, 2, 16])
        .attr({
          class: 'colored'
        })
        .transform(`t${config.PADDING},${config.PADDING}`);

      group.add(bkg);
      group.add(triangle);

      return group;
    }

  private getUnspecifiedIcon(): SNAPSVG_TYPE.Element {
    const group = this.canvas.g().attr({
      width: config.ICON_WIDTH + 2 * config.PADDING,
      height: config.ICON_HEIGHT + 2 * config.PADDING
    });

    const bkg = this.canvas
      .rect(
        0,
        0,
        config.ICON_WIDTH + 2 * config.PADDING,
        config.ICON_HEIGHT + 2 * config.PADDING,
        config.RECT_RADIUS,
        config.RECT_RADIUS
      )
      .attr({
        fill: 'white'
      });

    const triangle = this.canvas
      .polygon([0, 7, 25, 15, 0, 23])
      .attr({
        class: 'colored'
      })
      .transform(`t${config.PADDING},${config.PADDING}`);

    group.add(bkg);
    group.add(triangle);

    return group;
  }

  private getFixedIcon(): SNAPSVG_TYPE.Element {
    const group = this.canvas.g().attr({
      width: config.ICON_WIDTH + 2 * config.PADDING,
      height: config.ICON_HEIGHT + 2 * config.PADDING
    });

    const bkg = this.canvas
      .rect(
        0,
        0,
        config.ICON_WIDTH + 2 * config.PADDING,
        config.ICON_HEIGHT + 2 * config.PADDING,
        config.RECT_RADIUS,
        config.RECT_RADIUS
      )
      .attr({
        fill: 'white'
      });

    const triangle = this.canvas
      .polygon([0, 7, 25, 15, 0, 23])
      .attr({
        class: 'colored'
      })
      .transform(`t${config.PADDING},${config.PADDING}`);

    const line = this.canvas
      .line(0, 26, 25, 26)
      .attr({
        strokeWidth: 2,
        class: 'coloredStroke'
      })
      .transform(`t${config.PADDING},${config.PADDING}`);

    group.add(bkg);
    group.add(triangle);
    group.add(line);

    return group;
  }

  private getVariableIcon(): SNAPSVG_TYPE.Element {
    const group = this.canvas.g().attr({
      width: config.ICON_WIDTH + 2 * config.PADDING,
      height: config.ICON_HEIGHT + 2 * config.PADDING
    });

    const bkg = this.canvas
      .rect(
        0,
        0,
        config.ICON_WIDTH + 2 * config.PADDING,
        config.ICON_HEIGHT + 2 * config.PADDING,
        config.RECT_RADIUS,
        config.RECT_RADIUS
      )
      .attr({
        fill: 'white'
      });

    const triangle = this.canvas
      .polygon([0, 7, 25, 15, 0, 23])
      .attr({
        class: 'colored'
      })
      .transform(`t${config.PADDING},${config.PADDING}`);

    const line = this.canvas
      .line(5, 29, 20, 1)
      .attr({
        strokeWidth: 2,
        class: 'coloredStroke'
      })
      .transform(`t${config.PADDING},${config.PADDING}`);

    const arrow = this.canvas
      .polygon([13.5, 4.5, 20.8, -0.5, 21, 8])
      .attr({
        class: 'colored'
      })
      .transform('t(8 2)');

    group.add(bkg);
    group.add(triangle);
    group.add(line);
    group.add(arrow);

    return group;
  }

  private getStorageIcon(): SNAPSVG_TYPE.Element {
    const group = this.canvas.g().attr({
      width: config.ICON_WIDTH + 2 * config.PADDING,
      height: config.ICON_HEIGHT + 2 * config.PADDING
    });

    const bkg = this.canvas
      .rect(
        0,
        0,
        config.ICON_WIDTH + 2 * config.PADDING,
        config.ICON_HEIGHT + 2 * config.PADDING,
        config.RECT_RADIUS,
        config.RECT_RADIUS
      )
      .attr({
        fill: 'white'
      });

    const triangle = this.canvas
      .polygon([0, 7, 25, 23, 25, 7, 0, 23])
      .attr({
        class: 'colored'
      })
      .transform(`t${config.PADDING},${config.PADDING}`);

    group.add(bkg);
    group.add(triangle);

    return group;
  }
}
