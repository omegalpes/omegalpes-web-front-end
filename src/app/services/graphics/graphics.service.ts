import { Injectable } from '@angular/core';
import { IconsService } from './icons/icons.service';
import 'snapsvg-cjs';
import SNAPSVG_TYPE, { Paper } from 'snapsvg';
import { Shape, Direction, Position } from 'src/app/stores/canvas/canvas.state';
import * as configGraphics from './graphics.config';
import {
  UnitInstance,
  UnitInstanceParameter
} from 'src/app/models/UnitInstance';
import { UnitConfig } from 'src/app/models/Config';
import { EnergyType } from 'src/app/models/EnergyType';
import { Subtype } from 'src/app/models/Subtype';
import { ConfigService } from '../config/config.service';
import _get from 'lodash/get';

@Injectable({
  providedIn: 'root'
})
export class GraphicsService {
  private canvas: Paper;
  private masterGroup: SNAPSVG_TYPE.Element;

  constructor(
    private readonly iconsService: IconsService,
    private readonly configService: ConfigService
  ) {}

  public getGraphicUnitElement(
    unitInstance: UnitInstance,
    unitConfig: UnitConfig,
    canvas: Paper,
    masterGroup: SNAPSVG_TYPE.Element,
    position?: Position,
    childrenShapesToImport?: Shape[]
  ): Shape {
    const shape: Shape = {
      id: `shape_${unitInstance.id}`,
      position: position ? position : configGraphics.INITIAL_POSITION,
      connectors: [],
      unitInstanceId: unitInstance.id,
      children: []
    };
    let graphicElement: SNAPSVG_TYPE.Element;
    let childrenShapes: Shape[];
    this.canvas = canvas;
    this.masterGroup = masterGroup;

    const name =
      unitInstance.properties
        .find(parameter => parameter.name === 'name')
        .value.toString() || 'Node';

    if (this.isType(unitConfig, EnergyType.PRODUCTION)) {
      shape.globalDirection = Direction.OUT;
      shape.isNode = false;
      graphicElement = this.createOutputUnit(shape, name, unitConfig.subtype);
    } else if (this.isType(unitConfig, EnergyType.NODE)) {
      shape.globalDirection = Direction.BOTH;
      shape.isNode = true;
      graphicElement = this.createNodeUnit(shape, name);
    } else if (this.isType(unitConfig, EnergyType.CONVERSION)
      || this.isType(unitConfig, EnergyType.SINGLE)
      || this.isType(unitConfig, EnergyType.REVERSIBLE_CONVERSION)) {
      shape.isNode = false;
      graphicElement = this.createConversionUnit(shape, name, unitConfig.subtype);
      graphicElement.transform(`t${shape.position.x},${shape.position.y}`);
      if (childrenShapesToImport.length !== 0) {
        childrenShapes = this.getChildrenShapes(shape, unitInstance.children, true, childrenShapesToImport);
      } else {
        childrenShapes = this.getChildrenShapes(shape, unitInstance.children, false);
      }
    } else if (this.isType(unitConfig, EnergyType.REVERSIBLE)) {
      shape.globalDirection = Direction.BOTH;
      shape.isNode = false;
      graphicElement = this.createReversibleUnit(shape, name, unitConfig.subtype);
    } else {
      shape.globalDirection = Direction.IN;
      shape.isNode = false;
      graphicElement = this.createInputUnit(shape, name, unitConfig.subtype);
    }

    if (!this.isType(unitConfig, EnergyType.CONVERSION)) {
      graphicElement.transform(`t${shape.position.x},${shape.position.y}`);
    }
    graphicElement.addClass(
      unitInstance.properties['energy_type']
        ? unitInstance.properties['energy_type']
        : ''
    );

    shape.children = childrenShapes;
    shape.svg = graphicElement;
    return shape;
  }

  private createGroupUnit(shape: Shape): SNAPSVG_TYPE.Element {
    const group: SNAPSVG_TYPE.Element = this.canvas.g().attr({
      id: `${shape.id}`
    });
    group.addClass('element_group');
    this.masterGroup.add(group);

    return group;
  }

  private createTextUnit(
    label: string,
    posX: number,
    posY: number,
    textAnchor = 'start'
  ): SNAPSVG_TYPE.Element {
    const title: SNAPSVG_TYPE.Element = this.canvas.text(0, 0, label).attr({
      baselineShift: '-0.5ex',
      textAnchor
    });

    title.transform(`t${posX},${posY}`);

    return title;
  }

  public getContrastColor(color: string) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
    const rgb = {
              r: parseInt(result[1], 16),
              g: parseInt(result[2], 16),
              b: parseInt(result[3], 16)
          };


    return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000 > 125 ? 'black' : 'white';

  }

  private computeTotalWidthRect(content: SNAPSVG_TYPE.Element): number {
    const contentWidth = content.getBBox().width;
    return (
      contentWidth + configGraphics.ICON_WIDTH + 5 * configGraphics.PADDING
    );
  }

  private computeIconXPosition(
    background: SNAPSVG_TYPE.Element,
    direction: Direction
  ): number {
    if (direction === Direction.IN) {
      return configGraphics.PADDING;
    } else {
      return (
        background.getBBox().width -
        configGraphics.ICON_WIDTH -
        configGraphics.PADDING * 3
      );
    }
  }

  private computeRadius(content: SNAPSVG_TYPE.Element, outer = false): number {
    const contentWidth = content.getBBox().width;
    return outer
      ? contentWidth / 2 + configGraphics.NODE_MARGIN + configGraphics.PADDING
      : contentWidth / 2 +
          configGraphics.NODE_MARGIN +
          configGraphics.PADDING / 3;
  }

  private computeConnectorXPosition(
    background: SNAPSVG_TYPE.Element,
    direction: Direction
  ): number {
    const bbox = background.getBBox();
    if (direction === Direction.IN) {
      return bbox.x - configGraphics.CONNECTOR_SIZE / 2;
    } else if (direction === Direction.OUT) {
      return bbox.x + bbox.width - configGraphics.CONNECTOR_SIZE / 2;
    }
  }

  private createRectUnit(content: SNAPSVG_TYPE.Element, width?: number): SNAPSVG_TYPE.Element {
    const rect: SNAPSVG_TYPE.Element = this.canvas
      .rect(
        0,
        0,
        width ? width : this.computeTotalWidthRect(content),
        configGraphics.ICON_HEIGHT + 4 * configGraphics.PADDING,
        configGraphics.RECT_RADIUS,
        configGraphics.RECT_RADIUS
      )
      .attr({
        class: 'colored background'
      });

    return rect;
  }

  private createRect(width: number, height: number, position?: Position): SNAPSVG_TYPE.Element {
    return this.canvas
      .rect(
        0,
        0,
        width,
        height,
        configGraphics.RECT_RADIUS,
        configGraphics.RECT_RADIUS
      )
      .attr({
        class: 'coloredConversion background'
      });
  }

  private createCircleUnit(
    content: SNAPSVG_TYPE.Element
  ): SNAPSVG_TYPE.Element {
    const group = this.canvas.group().addClass('background');

    const contentWidth = content.getBBox().width;

    const outCircle = this.canvas
      .circle(
        0,
        0,
        contentWidth / 2 + configGraphics.NODE_MARGIN + configGraphics.PADDING
      )
      .attr({
        class: 'colored outCircle'
      });

    const circle: SNAPSVG_TYPE.Element = this.canvas
      .circle(
        0,
        0,
        contentWidth / 2 +
          configGraphics.NODE_MARGIN +
          configGraphics.PADDING / 3
      )
      .attr({
        stroke: 'white',
        strokeWidth: configGraphics.PADDING / 3,
        class: 'colored inCircle'
      });

    group.add(outCircle);
    group.add(circle);

    return group;
  }

  private createNodeUnit(shape: Shape, label: string): SNAPSVG_TYPE.Element {
    const group = this.createGroupUnit(shape);

    const title = this.createTextUnit(label, 0, 2.5, 'middle');

    const background = this.createCircleUnit(title);
    const radius = background.getBBox().width / 2;
    const connector = this.createConnector(
      shape,
      -radius - configGraphics.CONNECTOR_SIZE / 2,
      -configGraphics.CONNECTOR_SIZE / 2,
      Direction.IN
    );
    const connector2 = this.createConnector(
      shape,
      radius - configGraphics.CONNECTOR_SIZE / 2,
      -configGraphics.CONNECTOR_SIZE / 2,
      Direction.OUT
    );

    group.add(background);
    group.add(title);
    group.add(connector);
    group.add(connector2);

    return group;
  }

  private createInputUnit(
    shape: Shape,
    label: string,
    subtype: Subtype
  ): SNAPSVG_TYPE.Element {
    const group = this.createGroupUnit(shape);

    const title = this.createTextUnit(
      label,
      configGraphics.PADDING * 4 + configGraphics.ICON_WIDTH,
      configGraphics.TOTAL_HEIGHT / 2
    );

    const background = this.createRectUnit(title);
    group.add(background);
    const connector = this.createConnector(
      shape,
      this.computeConnectorXPosition(background, Direction.IN),
      (configGraphics.TOTAL_HEIGHT - configGraphics.CONNECTOR_SIZE) / 2,
      Direction.IN
    );

    const icon = this.iconsService
      .getIcon(this.canvas, subtype)
      .transform(
        `t${this.computeIconXPosition(background, Direction.IN)},${
          configGraphics.PADDING
        }`
      );
    group.add(icon);

    group.add(title);
    group.add(connector);

    return group;
  }

  private createOutputUnit(
    shape: Shape,
    label: string,
    subtype: Subtype
  ): SNAPSVG_TYPE.Element {
    const group = this.createGroupUnit(shape);

    const title = this.createTextUnit(
      label,
      configGraphics.PADDING,
      configGraphics.TOTAL_HEIGHT / 2
    );

    const background = this.createRectUnit(title);

    const connector = this.createConnector(
      shape,
      this.computeConnectorXPosition(background, Direction.OUT),
      (configGraphics.TOTAL_HEIGHT - configGraphics.CONNECTOR_SIZE) / 2,
      Direction.OUT
    );

    const icon = this.iconsService
      .getIcon(this.canvas, subtype)
      .transform(
        `t${this.computeIconXPosition(background, Direction.OUT)},${
          configGraphics.PADDING
        }`
      );

    group.add(background);
    group.add(icon);
    group.add(title);
    group.add(connector);

    return group;
  }

  private createReversibleUnit(
      shape: Shape,
      label: string,
      subtype: Subtype
  ): SNAPSVG_TYPE.Element {
    const group = this.createGroupUnit(shape);
    const title = this.createTextUnit(
      label,
      configGraphics.PADDING,
      configGraphics.TOTAL_HEIGHT / 2
    );

    const background = this.createRectUnit(title);

    const connector = this.createConnector(
      shape,
      this.computeConnectorXPosition(background, Direction.IN),
      (configGraphics.TOTAL_HEIGHT - configGraphics.CONNECTOR_SIZE) / 2,
      Direction.IN
    );

    const connector2 = this.createConnector(
          shape,
          this.computeConnectorXPosition(background, Direction.OUT),
          (configGraphics.TOTAL_HEIGHT - configGraphics.CONNECTOR_SIZE) / 2,
          Direction.OUT
        );

    const icon = this.iconsService
      .getIcon(this.canvas, subtype)
      .transform(
        `t${this.computeIconXPosition(background, Direction.OUT)},${
          configGraphics.PADDING
        }`
      );

    group.add(background);
    group.add(icon);
    group.add(title);
    group.add(connector);
    group.add(connector2);

    return group;
  }

  private createConversionUnit(
    globalShape: Shape,
    label: string,
    subtype: Subtype
  ): SNAPSVG_TYPE.Element {
    const group = this.createGroupUnit(globalShape);
    const title = this.createTextUnit(
        label,
      configGraphics.RECT_WIDTH / 2 - 25,
      configGraphics.PADDING * 4
    );
    const background = this.createRect(configGraphics.RECT_WIDTH, configGraphics.RECT_HEIGHT);
    group.add(background);
    group.add(title);
    return group;
  }


  private createConnector(
    shape: Shape,
    x: number,
    y: number,
    direction: Direction
  ): SNAPSVG_TYPE.Element {
    const connector = this.canvas
      .rect(
        0,
        0,
        configGraphics.CONNECTOR_SIZE,
        configGraphics.CONNECTOR_SIZE,
        3,
        3
      )
      .attr({
        fill: 'white',
        class: 'coloredStroke'
      })
      .transform(`t${x},${y}`);
    connector.addClass('connector');

    shape.connectors.push({
      direction,
      relativePosition: {
        x,
        y
      },
      links: [],
      svg: connector
    });

    connector.data(`connector_id`, shape.connectors.length - 1);
    connector.attr({
      'data-id': shape.connectors.length - 1
    });

    return connector;
  }

  private isType(unitConfig: UnitConfig, energyType: EnergyType): boolean {
    return !!unitConfig.types.find(
      type => type.toLowerCase() === energyType.toLowerCase()
    );
  }

  public updateEnergyType(shape: Shape, energyType: UnitInstanceParameter) {
    if (!energyType) {
      return;
    }

    if (!energyType.value) {
      return;
    }

    const element = shape.svg.node;

    this.configService.getModelConfig().subscribe(modelConfig => {
      modelConfig.energies.forEach(energy =>
        element.classList.remove(energy.energy)
      );
      element.classList.add(energyType.value.toString());
    });
  }

  public changeLabel(shape: Shape, label: string) {
    shape.svg.node.querySelector('text').textContent = label;
    if (shape.isNode) {
      this.resizeUnitCircle(shape);
    } else {
      // this.resizeUnitRect(shape);
    }
  }

  public resizeUnitRect(shape: Shape) {
    const textUnit = shape.svg.children().find(child => child.type === 'text');

    const background = shape.svg
      .children()
      .find(child => child.hasClass('background'));
    background.attr({
      width: this.computeTotalWidthRect(textUnit)
    });

    const icon = shape.svg.children().find(child => child.hasClass('icon'));
    icon.transform(
      `t${this.computeIconXPosition(background, shape.globalDirection)},${
        configGraphics.PADDING
      }`
    );

    this.updateConnectorsPosition(shape, background);
  }

  private resizeUnitCircle(shape: Shape) {
    const textUnit = shape.svg.children().find(child => child.type === 'text');

    const background = shape.svg
      .children()
      .find(child => child.hasClass('background'));

    const outCircle = background
      .children()
      .find(child => child.hasClass('outCircle'));
    outCircle.attr({
      r: this.computeRadius(textUnit, true)
    });

    const inCircle = background
      .children()
      .find(child => child.hasClass('inCircle'));
    inCircle.attr({
      r: this.computeRadius(textUnit)
    });

    this.updateConnectorsPosition(shape, outCircle);
  }

  private updateConnectorsPosition(
    shape: Shape,
    background: SNAPSVG_TYPE.Element
  ) {
    shape.connectors.forEach(connector => {
      connector.relativePosition.x = this.computeConnectorXPosition(
        background,
        connector.direction
      );
      connector.svg.transform(
        `t${connector.relativePosition.x},${connector.relativePosition.y}`
      );
    });
  }

  private getChildrenShapes(shape: Shape, children: UnitInstance[], isImported: boolean, shapesToImport?: Shape[]) {
    const tempShape: Shape[] = [];
    children.forEach((instance, index) => {
      const s: Shape = {
        id: `shape_${instance.id}`,
        position: isImported ? shapesToImport[index].position : configGraphics.INITIAL_POSITION,
        connectors: [],
        unitInstanceId: instance.id,
        isNode: false,
        isChild: true
      };
      const name = instance.properties.find(parameter => parameter.name === 'name')
          .value.toString() || instance.className;
      const tempGroup = this.createGroupUnit(s);
      const unitConfig = this.configService.findUnitConfigByClassname(
        instance.className
      );
      let element;
      if (_get(instance, 'types', []).includes('Reversible')) {
        element = this.createReversibleUnit(s, name, unitConfig.subtype);
      } else {
        element = this.createUnit(s, name, unitConfig.subtype, instance.types);
      }
      tempGroup.add(element);
      let transformX;
      let transformY;
      if (!isImported) {
        transformX = s.position.x + configGraphics.PADDING * 2;
        transformY = s.position.y + configGraphics.CONVERSION_PADDING + index * 65;
      } else {
        transformX = s.position.x;
        transformY = s.position.y;
      }
      tempGroup.transform(`t${transformX},${transformY}`);
      s.svg = tempGroup;
      tempShape.push(s);
    });
    return tempShape;
  }
  private createUnit(
    shape: Shape,
    label: string,
    subtype: Subtype,
    types?: string[]
  ): SNAPSVG_TYPE.Element {
    const group = this.createGroupUnit(shape);
    const title = this.createTextUnit(
      label,
      configGraphics.PADDING * 4 + configGraphics.ICON_WIDTH,
      configGraphics.TOTAL_HEIGHT / 2
    );

    const background = this.createRectUnit(title, 420);
    group.add(background);

    const direction = types ? types.includes('Consumption') ? Direction.IN : Direction.OUT : Direction.IN;
    const connector = this.createConnector(
      shape,
      this.computeConnectorXPosition(background, direction),
      (configGraphics.TOTAL_HEIGHT - configGraphics.CONNECTOR_SIZE) / 2,
      direction
    );

    const icon = this.iconsService
      .getIcon(this.canvas, subtype)
      .transform(
        `t${this.computeIconXPosition(background, direction)},${
          configGraphics.PADDING
        }`
      );
    group.add(icon);
    group.add(title);
    group.add(connector);
    return group;
  }
}
