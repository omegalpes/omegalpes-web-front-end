export const ICON_WIDTH = 25;
export const ICON_HEIGHT = 30;
export const PADDING = 6;
export const RECT_RADIUS = 5;
export const NODE_MARGIN = 5;
export const TOTAL_HEIGHT = ICON_HEIGHT + PADDING * 4;
export const CONNECTOR_SIZE = 12;
export const INITIAL_COLOR = '#d9d9d9';
export const INITIAL_POSITION = {
  x: 700,
  y: 300
};
export const RECT_WIDTH = 450;
export const RECT_HEIGHT = 250;
export const CONVERSION_PADDING = 50;
