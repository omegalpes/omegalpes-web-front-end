import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  constructor(private readonly snackBar: MatSnackBar) {}

  public openToast(message: string, action?: string, duration: number = 5000, className?: string) {
    this.snackBar.open(message, action, { duration,  panelClass: [className]}
    );
  }
}
