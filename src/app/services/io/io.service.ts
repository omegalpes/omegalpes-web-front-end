import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { CanvasState, Shape } from 'src/app/stores/canvas/canvas.state';
import { ModelObjectsState } from 'src/app/stores/model-objects/model-objects.state';
import { UnitInstance } from 'src/app/models/UnitInstance';
import { CanvasService } from '../canvas/canvas.service';
import { ClearCanvas } from 'src/app/stores/canvas/canvas.actions';
import { SetModelObjectsState } from 'src/app/stores/model-objects/model-objects.actions';
import { RemoveSelectedUnit } from 'src/app/stores/selected-unit/selected-unit.actions';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import JSZip from 'jszip';
import * as saveSvg from 'save-svg-as-png';

export interface ImportFileModel {
  modelObjectsStore: {
    modelParametersConfig: any[];
    unitsInstance: UnitInstance[];
  };
  shapesStore: Shape[];
}

@Injectable({
  providedIn: 'root'
})
export class IoService {

  constructor(
    private readonly http: HttpClient,
    private readonly store: Store,
    private readonly canvasService: CanvasService
  ) {}

  public importModel() {
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = '.ow';
    input.click();

    input.addEventListener('change', event => {
      if (!(event && event.target && event.target['files'])) {
        return;
      }
      const fileReader = new FileReader();
      fileReader.readAsText(event.target['files'][0]);

      const loadEvent = e => {
        const model: ImportFileModel = JSON.parse(e.target['result'].toString());
        this.loadCanvas(model);
        fileReader.removeEventListener('load', loadEvent);
      };

      fileReader.addEventListener('load', loadEvent);
    });
  }

  public loadCanvas(model: ImportFileModel) {
    this.cleanStoresAndCanvas();
    this.canvasService.resetPositionAndScale();
    this.canvasService.buildCanvasFromImport(model);
    this.store.dispatch(new SetModelObjectsState());
    this.store.dispatch(new SetModelObjectsState(model.modelObjectsStore));
  }

  public async saveModel(): Promise<void> {
    this.canvasService.enableZoom();
    const jsonStores = this.getStoresIntoJson();
    localStorage.setItem('model', jsonStores);
    const result = await JSON.parse(localStorage.getItem('model'));
  }

  public exportModel(): void {
    const jsonStores = this.getStoresIntoJson();
    const blob = new Blob([jsonStores], {
      type: 'application/json'
    });
    this.downLoadFile(blob, `OMEGAlpesWeb_model.ow`);
  }

  public cleanStoresAndCanvas() {
    this.store.dispatch([
      new ClearCanvas(),
      new SetModelObjectsState(),
      new RemoveSelectedUnit()
    ]);
    this.canvasService.cleanCanvas();
  }

  public exportAsImage() {
    saveSvg.saveSvgAsPng(
      document.getElementById('canvas'),
      'OMEGAlpes_model.png',
      {
        backgroundColor: 'white'
      }
    );
  }

  public async generateScript(model: object): Promise<void> {
    await this.http
      .post(`${environment.omegalpesWebBackEnd}/pythonscript`, model, {
        responseType: 'text'
      }).toPromise().then(result => {
        localStorage.setItem('script', result);
      });
  }

  public getScript() {
    return localStorage.getItem('script');
  }

  public async execute(model: object): Promise<void> {
    localStorage.setItem('result', '');
    localStorage.setItem('error', '');
    await this.http
      .post(`${environment.omegalpesWebBackEnd}/resolve-script`, model
      )
      .toPromise().then(result => {
        localStorage.setItem('result', JSON.stringify(result));
      }).catch(error => {
        if (error.status === 500) {
          localStorage.setItem('error', '"Un problème a été rencontré."');
        } else {
          localStorage.setItem('error', JSON.stringify(error.error));
        }
      });
  }

  public async executeScript(): Promise<void> {
    localStorage.setItem('result', '');
    localStorage.setItem('error', '');
    const headers = new HttpHeaders({'Content-Type': 'text/plain;'});
    await this.http
    .post(`${environment.omegalpesWebBackEnd}/execute-script`, this.getScript(), {
      headers
    }
    )
    .toPromise().then(result => {
      localStorage.setItem('result', JSON.stringify(result));
    }).catch(error => {
      if (error.status === 500) {
        localStorage.setItem('error', '"Un problème a été rencontré."');
      } else {
        localStorage.setItem('error', JSON.stringify(error.error));
      }
    } );
  }

  public generateModel(model: object): void {
    this.http
      .post(`${environment.omegalpesWebBackEnd}/pythonscript`, model, {
        responseType: 'blob'
      })
      .subscribe(response => {
        const pythonBlob = new Blob([response], {
          type: 'application/x-python-code'
        });

        const zip = new JSZip();
        this.getImageBlob().then(imageURI => {
          zip.file('OMEGAlpes_script.py', pythonBlob);
          zip.file(
            'OMEGAlpesWeb_model.ow',
            this.getJSONBlob(this.getStoresIntoJson())
          );
          zip.file(
            'OMEGAlpes_script_mode.json',
            this.getJSONBlob(JSON.stringify(model))
          );

          const idx = imageURI.indexOf('base64,') + 'base64,'.length;
          const content = imageURI.substring(idx);
          zip.file('OMEGAlpesWeb_model.png', content, { base64: true });

          this.downloadZip(zip);
        });
      });
  }

  private getImageBlob(): Promise<string> {
    return saveSvg.svgAsPngUri(document.getElementById('canvas'), {
      backgroundColor: 'white'
    });
  }

  private getStoresIntoJson(): string {
    const shapesStoreWithDuplicate = this.store.selectSnapshot(CanvasState.shapes);
    const childrenIds = [];
    shapesStoreWithDuplicate.forEach(shape => {
      if (shape.children) {
        shape.children.forEach(child => childrenIds.push(child.id));
        shape.children.map(child => {
          const transform: string = child.svg['_'].transform;
          const newPos = transform.substr(1).split(',');
          child.position.x = Number(newPos[0]);
          child.position.y = Number(newPos[1]);
        });
      }
    });
    const shapesStore = shapesStoreWithDuplicate.filter(
      shape => !childrenIds.includes(shape.id)
    );
    const modelObjectsStoreWithDuplicate = this.store.selectSnapshot(
      ModelObjectsState.modelToSend
    );

    const unitChildrenIds = [];
    modelObjectsStoreWithDuplicate.unitsInstance.forEach(unitInstance => {
      if (unitInstance.children) {
        unitInstance.children.forEach(child => unitChildrenIds.push(child.id));
      }
    });
    const unitFiltered = modelObjectsStoreWithDuplicate.unitsInstance.filter(
      shape => !unitChildrenIds.includes(shape.id)
    );
    const modelObjectsStore = modelObjectsStoreWithDuplicate;
    modelObjectsStore.unitsInstance = unitFiltered;
    return JSON.stringify({
      shapesStore,
      modelObjectsStore
    });
  }

  private getJSONBlob(json: string) {
    return new Blob([json], { type: 'application/json' });
  }

  private downloadZip(zip: JSZip) {
    zip.generateAsync({ type: 'blob' }).then(blob => {
      this.downLoadFile(blob, `OMEGAlpes-web_script_complete.zip`);
    });
  }

  private downLoadFile(blob: Blob, nameDownload: string) {
    const href = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a['href'] = href;
    a['download'] = nameDownload;
    a.click();
  }
}
