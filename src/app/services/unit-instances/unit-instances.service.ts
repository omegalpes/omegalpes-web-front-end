import { Injectable } from '@angular/core';
import {UnitConfig, UnitGroupsConfig, UnitParameterConfig} from 'src/app/models/Config';
import {
  UnitInstance,
  UnitInstanceParameter
} from 'src/app/models/UnitInstance';
import { Guid } from 'guid-typescript';
import { Store } from '@ngxs/store';
import { AddUnitInstance } from 'src/app/stores/model-objects/model-objects.actions';
import {ConfigService} from 'src/app/services/config/config.service';
import _get from 'lodash/get';

@Injectable({
  providedIn: 'root'
})
export class UnitInstancesService {
  unitGroups: UnitGroupsConfig[];

  constructor(
    private readonly store: Store,
    private readonly configService: ConfigService) {
    this.configService
      .getUnitGroupsConfig()
      .subscribe(unitGroups => (this.unitGroups = unitGroups));
  }

  public createUnitInstance(unit: UnitConfig): UnitInstance {
    const unitInstance: UnitInstance = {
      id: Guid.create().toString(),
      className: unit.classname,
      linksUnits: [],
      exportsToNode: [],
      properties: this.getParametersInstance(unit.parameters),
      objectives: [],
      constraints: [],
      children: unit.children ? this.createChildrenUnitInstance(unit.children) : []
    };
    unitInstance.properties.find(parameter => parameter.name === 'name').value =
      unit.label;
    this.store.dispatch(new AddUnitInstance(unitInstance));
    return unitInstance;
  }

  private createChildrenUnitInstance(children) {
    const childrenInstance: UnitInstance[] = [];
    const childrenConfig: UnitConfig[] = [];
    children.forEach(child => {
      this.unitGroups.forEach(unitGroup => {
        unitGroup.units.forEach(unit => {
          if (unit.classname === child.type) {
            childrenConfig.push(unit);
          }
        });
      });
    });
    childrenConfig.forEach((child, index) => {
      const instance: UnitInstance = {
        id: Guid.create().toString(),
        className: child['classname'],
        linksUnits: [],
        exportsToNode: [],
        properties: this.getParametersInstance(child.parameters),
        objectives: [],
        constraints: [],
        types: child.types
      };
      instance.properties.find(parameter => parameter.name === 'name').value =
        child.label;
      if (_get(children[index], 'energyType.energy', undefined) !== undefined) {
        instance.properties.find(parameter => parameter.name === 'energy_type').value =
          children[index].energyType.energy;
      }
      childrenInstance.push(instance);
    });
    return childrenInstance;
  }

  private getParametersInstance(
    parameters: UnitParameterConfig[]
  ): UnitInstanceParameter[] {
    return parameters
      ? parameters.map(parameter => ({
          name: parameter.name,
          type: parameter.type,
          value: parameter.default
        }))
      : [];
  }

  public getPropertyByName(
    unitInstance: UnitInstance,
    propertyName: string
  ): UnitInstanceParameter {
    return unitInstance.properties.find(
      property => property.name === propertyName
    );
  }
}
