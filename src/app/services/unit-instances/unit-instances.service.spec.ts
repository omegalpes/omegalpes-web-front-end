import { TestBed } from '@angular/core/testing';

import { UnitInstancesService } from './unit-instances.service';

describe('UnitInstancesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnitInstancesService = TestBed.get(UnitInstancesService);
    expect(service).toBeTruthy();
  });
});
