import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  Config,
  ModelConfig,
  UnitGroupsConfig,
  UnitConfig
} from 'src/app/models/Config';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  config$: Observable<Config>;
  config: Config;

  constructor(
    private readonly http: HttpClient,
    ) {
    this.config$ = this.http.get<Config>(
      `${environment.omegalpesWebBackEnd}/configfile/`
    );
    this.config$.subscribe(config => (this.config = config));
  }

  public getConfig(): Observable<Config> {
    return this.config
      ? new Observable(observer => {
          observer.next(this.config), observer.complete();
        })
      : this.config$;
  }

  public getModelConfig(): Observable<ModelConfig> {
    return this.config
      ? new Observable(observer => {
          observer.next(this.config.model), observer.complete();
        })
      : this.getConfig().pipe(map((data: Config) => data.model));
  }

  public getUnitGroupsConfig(): Observable<UnitGroupsConfig[]> {
    return this.config
      ? new Observable(observer => {
          observer.next(this.config.unit_groups), observer.complete();
        })
      : this.getConfig().pipe(map((data: Config) => data.unit_groups));
  }

  public findUnitConfigByClassname(classname: string): UnitConfig {
    let unitFound: UnitConfig;
    this.config.unit_groups.forEach(unitGroup =>
      unitGroup.units.forEach(unit => {
        if (unit.classname === classname) {
          unitFound = unit;
        }
      })
    );
    return unitFound;
  }



  public getColorByEnergyType(energyType: string): string {
    const energy = this.config.model.energies.find(
      energyToFind => energyToFind.energy === energyType
    );
    return energy ? energy.color : undefined;
  }
}
