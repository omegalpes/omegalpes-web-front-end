import {Injectable} from '@angular/core';
import {CanvasState, Connector, Direction, Link, Shape} from 'src/app/stores/canvas/canvas.state';
import {ConfigService} from '../config/config.service';
import {Subtype} from 'src/app/models/Subtype';
import {UnitConfig} from 'src/app/models/Config';
import {Store} from '@ngxs/store';
import {ModelObjectsState} from 'src/app/stores/model-objects/model-objects.state';
import {UnitInstancesService} from '../unit-instances/unit-instances.service';
import {UnitInstance} from 'src/app/models/UnitInstance';
import {UpdateUnitInstance} from 'src/app/stores/model-objects/model-objects.actions';

enum NODE_POSITION {
  SOURCE,
  DESTINATION,
  NONE,
  BOTH
}

@Injectable({
  providedIn: 'root'
})
export class LinkResolverService {
  constructor(
    private readonly configService: ConfigService,
    private readonly unitInstancesService: UnitInstancesService,
    private readonly store: Store
  ) {}

  public createNewLink(shapeSource: Shape, shapeDestination: Shape, connector: Connector): Link {
    const nodePosition = this.whereAreNodes(shapeSource, shapeDestination, connector);

    if (this.linkExists(shapeSource, shapeDestination,connector)) {
      throw new Error('Link already exists');
    }

    if (shapeSource === shapeDestination) {
      throw new Error('You cannot link element with himself');
    }

    if (nodePosition === NODE_POSITION.NONE) {
      throw new Error('Source and Destination are incompatible');
    }

    let inputShape: Shape;
    let outputShape: Shape;

    if (nodePosition === NODE_POSITION.SOURCE) {
      inputShape = this.isInput(shapeDestination)
        ? shapeDestination
        : shapeSource;
      outputShape = this.isOutput(shapeDestination)
        ? shapeDestination
        : shapeSource;
    } else if (nodePosition === NODE_POSITION.DESTINATION) {
      inputShape = this.isInput(shapeSource) ? shapeSource : shapeDestination;
      outputShape = this.isOutput(shapeSource) ? shapeSource : shapeDestination;
    } else if (nodePosition === NODE_POSITION.BOTH) {
      outputShape = shapeSource;
      inputShape = shapeDestination;
    }

    this.autoAssignEnergy(
      inputShape.unitInstanceId,
      outputShape.unitInstanceId
    );

    return {
      inputShapeId: inputShape.id,
      outputShapeId: outputShape.id
    };
  }

  public autoAssignEnergy(inputShapeId: string, outputShapeId: string) {
    const inputUnitInstance: UnitInstance = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceById(inputShapeId)
    );
    const outputUnitInstance: UnitInstance = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceById(outputShapeId)
    );
    let inputProp = this.unitInstancesService.getPropertyByName(
      inputUnitInstance,
      'energy_type'
    );
    let outputProp = this.unitInstancesService.getPropertyByName(
      outputUnitInstance,
      'energy_type'
    );
    if (this.isReversible(this.configService.findUnitConfigByClassname(outputUnitInstance.className))) {
       outputProp = this.unitInstancesService.getPropertyByName(
        outputUnitInstance,
        'energy_type_prod'
      );
       inputProp = this.unitInstancesService.getPropertyByName(
        inputUnitInstance,
        'energy_type'
      );
    }

    if (inputProp && (!inputProp.value || inputProp.value === '')) {
      inputProp.value = outputProp.value;
      const unitInstanceToUpdate = {
        ...inputUnitInstance
      };
      this.store.dispatch(new UpdateUnitInstance(unitInstanceToUpdate));
    }
  }

  private whereAreNodes(
    shapeSource: Shape,
    shapeDestination: Shape,
    connector: Connector
  ): NODE_POSITION {
    const shapeSourceUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(shapeSource.unitInstanceId)
    );
    const shapeDestinationUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(
        shapeDestination.unitInstanceId
      )
    );
    const unitConfigSource = this.configService.findUnitConfigByClassname(
      shapeSourceUnitInstanceClassname
    );
    const unitConfigDestination = this.configService.findUnitConfigByClassname(
      shapeDestinationUnitInstanceClassname

    );

    if ( !(this.isANode(unitConfigSource) || this.isANode(unitConfigDestination)) ) {
      return NODE_POSITION.NONE;
    } else if ( this.isANode(unitConfigSource) && this.isANode(unitConfigDestination) ) {
      return NODE_POSITION.BOTH;
    } else if ( this.isReversible(unitConfigSource) ) {
        if (connector.direction === Direction.OUT) {
          return NODE_POSITION.SOURCE;
        }
        if (connector.direction === Direction.IN) {
          return NODE_POSITION.DESTINATION;
        }
    } else if ( this.isReversible(unitConfigDestination) ) {
      if (connector.direction === Direction.OUT) {
        return NODE_POSITION.SOURCE;
      }
      if (connector.direction === Direction.IN) {
        return NODE_POSITION.DESTINATION;
      }
    } else if (this.isANode(unitConfigSource)) {
      return NODE_POSITION.SOURCE;
    }

    return NODE_POSITION.DESTINATION;
  }

  private isInput(shape: Shape): boolean {
    return  shape.connectors[0].direction === Direction.IN;
  }

  private isReversible(unitConfig: UnitConfig): boolean {
   return unitConfig.subtype === Subtype.REVERSIBLE;
  }

  private isOutput(shape: Shape): boolean {
    return shape.connectors[0].direction === Direction.OUT;
  }

  private isANode(unitConfig: UnitConfig) {
    return unitConfig.subtype === Subtype.NODE;
  }

  private findConnector(shape: Shape, direction: Direction): Connector {
    return shape.connectors.find(
      connector => connector.direction === direction
    );
  }

  private linkExists(shape1: Shape, shape2: Shape,connector : Connector) {
    const shapeSource = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(shape1.unitInstanceId)
    );
    const shapeDestination = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(shape2.unitInstanceId)
    );
    const unitConfigSource = this.configService.findUnitConfigByClassname(
      shapeSource
    );
    const unitConfigDestination = this.configService.findUnitConfigByClassname(
      shapeDestination
    );

    if (this.isReversible(unitConfigDestination)||this.isReversible(unitConfigSource)){
        return !!connector.links.find(link => {
            const inputShape = this.store.selectSnapshot(
              CanvasState.shapeById(link.inputShapeId)
            );
            const outputShape = this.store.selectSnapshot(
              CanvasState.shapeById(link.outputShapeId)
            );
            return(
              (inputShape === shape1 && outputShape === shape2) ||
              (inputShape === shape2 && outputShape === shape1)
            );
          })
        }else{
      return !!shape1.connectors.reduce(
      (found, connector) =>
        found ||
        !!connector.links.find(link => {
          const inputShape = this.store.selectSnapshot(
            CanvasState.shapeById(link.inputShapeId)
          );
          const outputShape = this.store.selectSnapshot(
            CanvasState.shapeById(link.outputShapeId)
          );
          return (

            (inputShape === shape1 && outputShape === shape2) ||
            (inputShape === shape2 && outputShape === shape1)
          );
        }),
      false
    );
  }
  }
}
