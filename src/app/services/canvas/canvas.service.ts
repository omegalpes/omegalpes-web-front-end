import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import {
  MoveShape,
  RemoveShape,
  AddLink,
  RemoveLink,
  AddShape
} from 'src/app/stores/canvas/canvas.actions';
import {
  Shape,
  Connector,
  Link,
  Direction,
  Position,
  CanvasState
} from 'src/app/stores/canvas/canvas.state';
import 'snapsvg-cjs';
import SNAPSVG_TYPE, { Paper } from 'snapsvg';
import { GraphicsService } from '../graphics/graphics.service';
import * as graphicConfig from '../graphics/graphics.config';
import { UnitInstance } from 'src/app/models/UnitInstance';
import {
  AddSelectedUnit,
  RemoveSelectedUnit
} from 'src/app/stores/selected-unit/selected-unit.actions';

import { LinkResolverService } from '../link-resolver/link-resolver.service';
import { ToasterService } from '../toaster/toaster.service';
import { ImportFileModel } from '../io/io.service';
import { MatDialog } from '@angular/material';
import { DialogRemoveComponent } from 'src/app/commons/dialog-remove/dialog-remove.component';
import {
  RemoveLinkToNode,
  AddUnitInstance
} from 'src/app/stores/model-objects/model-objects.actions';
import { ModelObjectsState } from 'src/app/stores/model-objects/model-objects.state';
import { ConfigService } from '../config/config.service';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import _ from 'lodash';
declare var Snap: typeof SNAPSVG_TYPE;

enum DRAGMODE {
  CANVAS,
  ELEMENT,
  LINK
}

@Injectable({
  providedIn: 'root'
})
export class CanvasService {
  private canvas: Paper;
  private masterGroup: SNAPSVG_TYPE.Element;
  private masterGroupPosition: Position = {
    x: 0,
    y: 0
  };
  private lastMouseUpShape: Shape;
  private marker: SNAPSVG_TYPE.Element;
  private initialPosition: Position;
  private dragMode: DRAGMODE = DRAGMODE.CANVAS;
  private scale = 1;
  private destroyed$: Subject<void>;
  private zoomDisabled: boolean;

  constructor(
    private readonly store: Store,
    private readonly graphicsService: GraphicsService,
    private readonly linkResolverService: LinkResolverService,
    private readonly toasterService: ToasterService,
    private readonly dialogService: MatDialog,
    private readonly configService: ConfigService
  ) {
    this.zoomDisabled = false;
  }

  public getCanvas(): Paper {
    return this.canvas;
  }

  public setCanvasElement(): void {
    this.destroyed$ = new Subject();
    this.canvas = Snap('#canvas');
    const arrow = this.canvas.polygon([0, 0, 8, 4, 0, 8]).attr({
      fill: 'black'
    });
    this.masterGroup = this.canvas.g().attr({
      id: 'masterGroup'
    });

    this.generateEnergyCSS();

    this.setNavigationCanvasEvent();

    this.marker = arrow.marker(0, 0, 10, 10, 7, 4);
  }

  private generateEnergyCSS() {
    const styleEl = document.createElement('style');
    styleEl.appendChild(document.createTextNode(''));
    document.head.appendChild(styleEl);

    const sheet = styleEl.sheet as CSSStyleSheet;

    this.configService.getModelConfig().subscribe(modelConfig => {
      modelConfig.energies.forEach(energy => {
        try {
          sheet.insertRule(
            `.${energy.energy} .colored { fill: ${energy.color}}`
          );
          sheet.insertRule(
            `.${energy.energy} .coloredStroke { stroke: ${energy.color}}`
          );
          sheet.insertRule(
            `.${energy.energy} text { fill: ${this.graphicsService.getContrastColor(energy.color)}}`
          );
        } catch (error) {
          console.error(error);
        }
      });
    });
  }



  private setNavigationCanvasEvent() {
    let posX = 0;
    let posY = 0;
    this.canvas.drag(
      // OnMove
      (dx, dy, x, y) => {
        if (this.dragMode === DRAGMODE.CANVAS) {
          posX = this.masterGroupPosition.x + dx;
          posY = this.masterGroupPosition.y + dy;
          this.masterGroup.transform(
            `t${posX},${posY}s${this.scale},${this.scale}`
          );
        }
      },
      // OnStart
      (x, y, event) => {},
      // OnStop
      event => {
        this.masterGroupPosition = {
          x: posX,
          y: posY
        };
      }
    );

    window.addEventListener('wheel', event => {
      if (!this.zoomDisabled) {
        if (event.deltaY < 0) {
          this.scale = this.scale < 3 ? this.scale + 0.2 : this.scale;
        } else {
          this.scale = this.scale > 0.4 ? this.scale - 0.2 : this.scale;
        }
        this.masterGroup.transform(
          `t${this.masterGroupPosition.x},${this.masterGroupPosition.y}s${this.scale},${this.scale}`
        );
      }
    });
  }

  public buildCanvasFromImport(importModel: ImportFileModel) {
    const linksToCreate: Link[] = [];
    const shapes: Shape[] = [];
    importModel.modelObjectsStore.unitsInstance.forEach(unitInstance => {
      this.store.dispatch(new AddUnitInstance(unitInstance));

      const shapeToImport = importModel.shapesStore.find(
        shape => shape.unitInstanceId === unitInstance.id
      );

      shapeToImport.connectors.forEach((connector, index) => {
        connector.links.forEach(link => {
          if (
            !linksToCreate.find(
              linkToCreate =>
                linkToCreate.inputShapeId === link.inputShapeId &&
                linkToCreate.outputShapeId === link.outputShapeId
            )
          ) {
            linksToCreate.push(link);
          }
        });
      });

      shapes.push(
        this.addShape({
          unitInstance,
          shapeToImport
        })
      );
    });
    let switchConnector = true
    linksToCreate.forEach(linkToCreate => {
      if (shapes.find(shape => shape.id === linkToCreate.inputShapeId) && shapes.find(shape => shape.id === linkToCreate.outputShapeId)) {
        if ((shapes.find(shape => shape.id === linkToCreate.inputShapeId).connectors.length) === 1) {
          const link = this.linkResolverService.createNewLink(
            shapes.find(shape => shape.id === linkToCreate.outputShapeId),
            shapes.find(shape => shape.id === linkToCreate.inputShapeId),
            shapes.find(shape => shape.id === linkToCreate.inputShapeId).connectors[0],
          );
          this.store.dispatch(new AddLink(link));
          this.displayLink(link);
        } else {
          if (switchConnector) {
            const link = this.linkResolverService.createNewLink(
              shapes.find(shape => shape.id === linkToCreate.inputShapeId),
              shapes.find(shape => shape.id === linkToCreate.outputShapeId),
              shapes.find(shape => shape.id === linkToCreate.inputShapeId).connectors[0],
            );
            this.store.dispatch(new AddLink(link));
            this.displayLink(link);
            switchConnector = false;
          } else {
            const link = this.linkResolverService.createNewLink(
              shapes.find(shape => shape.id === linkToCreate.outputShapeId),
              shapes.find(shape => shape.id === linkToCreate.inputShapeId),
              shapes.find(shape => shape.id === linkToCreate.outputShapeId).connectors[1],
            );
            this.store.dispatch(new AddLink(link));
            this.displayLink(link);
          }
        }
      } else if (shapes.find(shape => shape.id === linkToCreate.inputShapeId)) {
        const parentShapes = shapes.filter(shape => shape.children);
        const parentShape = parentShapes.find(shape => shape.children.find(child => child.id === linkToCreate.outputShapeId));
        const shapeDestination = parentShape.children.find(ch => ch.id === linkToCreate.outputShapeId);
        const connector = parentShape.children.find(ch => ch.id === linkToCreate.outputShapeId).connectors[0];
        const link = this.linkResolverService.createNewLink(
          shapes.find(shape => shape.id === linkToCreate.inputShapeId),
          shapeDestination,
          connector
        );
        this.store.dispatch(new AddLink(link));
        this.displayLink(link);
      } else if (shapes.find(shape => shape.id === linkToCreate.outputShapeId)) {
        const parentShapes = shapes.filter(shape => shape.children);
        const parentShape = parentShapes.find(shape => shape.children.find(child => child.id === linkToCreate.inputShapeId));
        const shapeSource = parentShape.children.find(ch => ch.id === linkToCreate.inputShapeId);
        const connector = parentShape.children.find(ch => ch.id === linkToCreate.inputShapeId).connectors[0];
        const link = this.linkResolverService.createNewLink(
          shapeSource,
          shapes.find(shape => shape.id === linkToCreate.outputShapeId),
          connector,
        );
        this.store.dispatch(new AddLink(link));
        this.displayLink(link);
      }
    });
  }

  public resetPositionAndScale() {
    this.masterGroupPosition = {
      x: 0,
      y: 0
    };

    this.scale = 1;

    this.masterGroup.transform(
      `t${this.masterGroupPosition.x},${this.masterGroupPosition.y}s${this.scale},${this.scale}`
    );
  }

  public addShape(parameters: {
    unitInstance: UnitInstance;
    shapeToImport?: Shape;
  }): Shape {
    const { unitInstance, shapeToImport } = parameters;

    const position: Position = shapeToImport
      ? shapeToImport.position
      : undefined;

    const hasChildren = _.get(unitInstance, 'children', false)
      && _.get(shapeToImport, 'children', false);

    const shape = this.graphicsService.getGraphicUnitElement(
      unitInstance,
      this.configService.findUnitConfigByClassname(unitInstance.className),
      this.canvas,
      this.masterGroup,
      position,
      hasChildren ? shapeToImport.children : []
    );

    if (shape.children) {
      shape.children.forEach(child => {
        this.setClickEvents(child);
        this.setUpdateEvents(child);
        this.setDragEvents(shape);
        this.setDragEventsOnChildren(child, shape);
        this.store.dispatch(new AddShape(child));
      });
    } else {
      this.setDragEvents(shape);
    }
    this.setClickEvents(shape);
    this.setUpdateEvents(shape);

    this.store.dispatch(new AddShape(shape));

    return shape;
  }

  private setUpdateEvents(shape: Shape) {
    this.store
      .select(ModelObjectsState.unitsInstanceById(shape.unitInstanceId))
      .pipe(filter(unitInstance => !!unitInstance), takeUntil(this.destroyed$))
      .subscribe(unitInstance => {
        if (shape.children) {
          shape.children.forEach(childShape => {
            const childUnitInstance = this.store.selectSnapshot(
              ModelObjectsState.unitsInstanceById(childShape.unitInstanceId)
            );
          });
        }
        this.graphicsService.updateEnergyType(
          shape,
          unitInstance.properties.find(prop => prop.name === 'energy_type')
        );
        this.graphicsService.changeLabel(
          shape,
          unitInstance.properties
            .find(prop => prop.name === 'name')
            .value.toString()
        );
        shape.connectors.forEach(connector => connector.links.forEach(link => this.updateLinkSvg(link)));
        if (shape.children) {
          shape.children.forEach(child => {
            child.connectors.forEach(connector => connector.links.forEach(link => this.updateLinkSvg(link)));
          });
        }
      });
  }

  private setDragEvents(shape: Shape) {
    let posX = shape.position.x;
    let posY = shape.position.y;
    const shapeSvg = shape.svg;

    let line: SNAPSVG_TYPE.Element;

    let activeConnector: Connector;

    shapeSvg.mouseup(event => (this.lastMouseUpShape = shape));

    shapeSvg.drag(
      // OnMove
      (dx, dy, x, y) => {
        if (this.dragMode === DRAGMODE.ELEMENT) {
          ({ posX, posY } = this.moveElement(
            posX,
            shape,
            dx,
            posY,
            dy,
            shapeSvg
          ));
        } else if (this.dragMode === DRAGMODE.LINK) {
          this.changeEndLine(line, posX, activeConnector, dx, posY, dy);
        }
      },
      // OnStart
      (x, y, event) => {
        if (event.button !== 0) {
          return;
        }
        const target = event.target as SVGElement;
        if (this.isConnector(target)) {
          ({ activeConnector, line } = this.createTemporaryLine(
            activeConnector,
            shape,
            target,
            line,
            posX,
            posY
          ));

          this.dragMode = DRAGMODE.LINK;
        } else {
          this.initialPosition = shape.position;
          this.dragMode = DRAGMODE.ELEMENT;
        }
      },
      // OnEnd
      event => {
        if (this.dragMode === DRAGMODE.LINK) {
          // On a relaché sur un élément
          this.canvas.removeClass('createLink');
          if (this.lastMouseUpShape) {
            try {
              const link = this.linkResolverService.createNewLink(
                shape,
                this.lastMouseUpShape,
                activeConnector
              );
              this.store.dispatch(new AddLink(link));
              this.displayLink(link);
            } catch (error) {
              this.toasterService.openToast(error.message);
            }
          }
          line.remove();
        }
        this.dragMode = DRAGMODE.CANVAS;
      }
    );
  }

  private setDragEventsOnChildren(shape: Shape, parentShape: Shape) {
    const shapeSvg = shape.svg;
    let line: SNAPSVG_TYPE.Element;
    let activeConnector: Connector;

    shapeSvg.mouseup(event => (this.lastMouseUpShape = shape));

    shapeSvg.drag(
      // OnMove
      (dx, dy, x, y) => {
        if (this.dragMode === DRAGMODE.LINK) {
          const childPos = shape.svg['_'].transform.substr(1).split(',');
          this.changeEndLine(line, Number(childPos[0]), activeConnector, dx, Number(childPos[1]), dy);
        }
      },
      // OnStart
      (x, y, event) => {
        if (event.button !== 0) {
          return;
        }
        const target = event.target as SVGElement;
        const childPos = shape.svg['_'].transform.substr(1).split(',');
        if (this.isConnector(target)) {
          ({ activeConnector, line } = this.createTemporaryLine(
            activeConnector,
            shape,
            target,
            line,
            Number(childPos[0]),
            Number(childPos[1])
          ));

          this.dragMode = DRAGMODE.LINK;
        } else {
          this.initialPosition = shape.position;
          this.dragMode = DRAGMODE.ELEMENT;
        }
      },
      // OnEnd
      event => {
        if (this.dragMode === DRAGMODE.LINK) {
          // On a relaché sur un élément
          this.canvas.removeClass('createLink');
          if (this.lastMouseUpShape) {
            try {
              const link = this.linkResolverService.createNewLink(
                shape,
                this.lastMouseUpShape,
                shape.connectors[0]
              );
              this.store.dispatch(new AddLink(link));
              this.displayLink(link);
            } catch (error) {
              this.toasterService.openToast(error.message);
            }
          }
          line.remove();
        }
        this.dragMode = DRAGMODE.CANVAS;
      }
    );
  }

  public removeShape(shape: Shape): void {
    shape.connectors.forEach(connector =>
      connector.links.forEach(link => {
        this.removeLinkSvg(link);
      })
    );
    if (shape.children) {
      shape.children.forEach(child => {
        child.connectors.forEach(connector => {
          connector.links.forEach(link => {
            this.removeLinkSvg(link);
          });
        });
      });
    }
    shape.svg.remove();
    if (shape.children) {
      shape.children.forEach(child => {
        child.svg.remove();
        this.store.dispatch(new RemoveShape(child.id));
      });
    }

    this.store.dispatch(new RemoveSelectedUnit());
    this.store.dispatch(new RemoveShape(shape.id));
  }

  private setClickEvents(shape: Shape) {
    shape.svg.click(() => {
      this.store.dispatch(new AddSelectedUnit(shape));
    }, null);
  }

  private createTemporaryLine(
    activeConnector: Connector,
    shape: Shape,
    target: SVGElement,
    line: SNAPSVG_TYPE.Element,
    posX: number,
    posY: number,
  ) {
    this.canvas.addClass('createLink');
    this.lastMouseUpShape = undefined;
    activeConnector = shape.connectors[target.dataset.id];
    line = this.canvas.line(
      activeConnector.relativePosition.x +
        graphicConfig.CONNECTOR_SIZE / 2 +
        posX,
      activeConnector.relativePosition.y +
        graphicConfig.CONNECTOR_SIZE / 2 +
        posY,
      activeConnector.relativePosition.x +
        graphicConfig.CONNECTOR_SIZE / 2 +
        posX,
      activeConnector.relativePosition.y +
        graphicConfig.CONNECTOR_SIZE / 2 +
        posY
    );
    line.attr({
      stroke: graphicConfig.INITIAL_COLOR,
      strokeWidth: '4',
      strokeDasharray: line.getTotalLength(),
      strokeDashoffset: 1
    });
    this.masterGroup.add(line);
    return { activeConnector, line };
  }

  private isConnector(target: SVGElement) {
    return target.classList.contains('connector');
  }

  private changeEndLine(
    line: SNAPSVG_TYPE.Element,
    posX: number,
    activeConnector: Connector,
    dx: number,
    posY: number,
    dy: number
  ) {
    line.attr({
      x2:
        posX +
        activeConnector.relativePosition.x +
        graphicConfig.CONNECTOR_SIZE / 2 +
        +dx / this.scale -
        3,
      y2:
        posY +
        activeConnector.relativePosition.y +
        graphicConfig.CONNECTOR_SIZE / 2 +
        dy / this.scale,
      strokeDasharray: line.getTotalLength()
    });
  }

  private moveElement(
    posX: number,
    shape: Shape,
    dx: number,
    posY: number,
    dy: number,
    shapeSvg: SNAPSVG_TYPE.Element
  ) {
    posX = this.initialPosition.x + dx / this.scale;
    posY = this.initialPosition.y + dy / this.scale;
    shapeSvg.transform(`t${posX},${posY}`);
    if (shape.children) {
      shape.children.forEach((c, index) => {
        c.svg.transform(`t${posX + 10},${posY + graphicConfig.CONVERSION_PADDING + index * 65}`);
        this.store.dispatch(new MoveShape(c.id, posX, posY));
      });
    }
    this.store.dispatch(new MoveShape(shape.id, posX, posY));
    shape.connectors.forEach(connector =>
      connector.links.forEach(link => {
        this.updateLinkSvg(link);
      })
    );
    if (shape.children) {
      shape.children.forEach(child => {
        child.connectors.forEach(connector =>
          connector.links.forEach(link => {
            this.updateLinkSvg(link);
          })
        );
      });
    }
    return { posX, posY };
  }

  private displayLink(link: Link) {
    const inputShape = this.store.selectSnapshot(
      CanvasState.shapeById(link.inputShapeId)
    );
    const outputShape = this.store.selectSnapshot(
      CanvasState.shapeById(link.outputShapeId)
    );
    const { start, stop } = this.coordinatesLinkComputation(
      inputShape,
      outputShape
    );

    link.svg = this.canvas.g().attr({
      class: 'link'
    });

    const backLine = this.canvas.line(start.x, start.y, stop.x, stop.y).attr({
      class: 'backArrow'
    });

    const visibleLine = this.canvas
      .line(start.x, start.y, stop.x, stop.y)
      .attr({
        stroke: 'black',
        strokeWidth: 2,
        markerEnd: this.marker,
        class: 'core'
      });

    link.svg.add(backLine);
    link.svg.add(visibleLine);
    this.masterGroup.add(link.svg);

    this.addLinkDoubleClickHandler(link);
  }

  private addLinkDoubleClickHandler(link: Link) {
    link.svg.dblclick(() => {
      const inputShape = this.store.selectSnapshot(
        CanvasState.shapeById(link.inputShapeId)
      );
      const outputShape = this.store.selectSnapshot(
        CanvasState.shapeById(link.outputShapeId)
      );
      const outputUnitInstance = this.store.selectSnapshot(
        ModelObjectsState.unitsInstanceById(outputShape.unitInstanceId)
      );
      const inputUnitInstance = this.store.selectSnapshot(
        ModelObjectsState.unitsInstanceById(inputShape.unitInstanceId)
      );

      this.dialogService
        .open(DialogRemoveComponent, {
          width: '16rem',
          data: `Would you remove link between ${
            outputUnitInstance.properties.find(prop => prop.name === 'name')
              .value
          } and ${
            inputUnitInstance.properties.find(prop => prop.name === 'name')
              .value
          } ?`
        })
        .afterClosed()
        .subscribe(result => {
          if (result) {
            this.removeLinkSvg(link);
            this.store.dispatch(new RemoveLink(link));
          }
        });
    });
  }

  private removeLinkSvg(link: Link) {
    const inputShape = this.store.selectSnapshot(
      CanvasState.shapeById(link.inputShapeId)
    );
    const outputShape = this.store.selectSnapshot(
      CanvasState.shapeById(link.outputShapeId)
    );
    link.svg.remove();
    this.store.dispatch(new RemoveLinkToNode(inputShape, outputShape));
  }

  private updateLinkSvg(link: Link) {
    const inputShape = this.store.selectSnapshot(
      CanvasState.shapeById(link.inputShapeId)
    );
    const outputShape = this.store.selectSnapshot(
      CanvasState.shapeById(link.outputShapeId)
    );
    const { start, stop } = this.coordinatesLinkComputation(
      inputShape,
      outputShape
    );

    link.svg.children().forEach(element => {
      element.attr({
        x1: start.x,
        y1: start.y,
        x2: stop.x,
        y2: stop.y
      });
    });
  }

  private coordinatesLinkComputation(inputShape: Shape, outputShape: Shape) {
    const outputConnector = outputShape.connectors.find(
      connector => connector.direction === Direction.OUT
    );
    const inputConnector = inputShape.connectors.find(
      connector => connector.direction === Direction.IN
    );

    const outputPos = outputShape.svg['_'].transform.substr(1).split(',');
    const start = {
      x:
       Number(outputPos[0]) +
        outputConnector.relativePosition.x +
        graphicConfig.CONNECTOR_SIZE / 2,
      y:
        Number(outputPos[1]) +
        outputConnector.relativePosition.y +
        graphicConfig.CONNECTOR_SIZE / 2
    };

    const childPos = inputShape.svg['_'].transform.substr(1).split(',');
    const stop = {
      x:
        Number(childPos[0]) +
        inputConnector.relativePosition.x +
        graphicConfig.CONNECTOR_SIZE / 2,
      y:
        Number(childPos[1]) +
        inputConnector.relativePosition.y +
        graphicConfig.CONNECTOR_SIZE / 2
    };

    return { start, stop };
  }

  public disableZoom() {
    this.zoomDisabled = true;
  }

  public enableZoom() {
    this.zoomDisabled = false;
  }

  public cleanCanvas() {
    this.canvas.clear();
    this.destroyed$.next();
    this.destroyed$.complete();
    this.setCanvasElement();
  }
}
