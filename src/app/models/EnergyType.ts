export enum EnergyType {
  PRODUCTION = 'production',
  CONSUMPTION = 'consumption',
  STORAGE = 'storage',
  CONVERSION = 'conversion',
  SINGLE = 'single',
  REVERSIBLE = 'reversible',
  REVERSIBLE_CONVERSION = 'reversibleconversion',
  NODE = 'node'
}
