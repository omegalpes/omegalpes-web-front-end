import { EnergyType } from './EnergyType';
import { Subtype } from './Subtype';

export interface Config {
  version: string;
  model: ModelConfig;
  unit_groups: UnitGroupsConfig[];
}

export interface ModelConfig {
  parameters: ModelParameterConfig[];
  energies: EnergyConfig[];
}

export interface ModelParameterConfig {
  name: string;
  label: string;
  type: string;
  default: string | number | null;
  value: string | number | null;
}

export interface EnergyConfig {
  energy: string;
  color: string;
}

export interface UnitGroupsConfig {
  group_name: string;
  units: UnitConfig[];
}

export interface UnitConfig {
  label: string;
  classname: string;
  types: EnergyType[];
  subtype: Subtype;
  energies: EnergyConfig[];
  parameters: UnitParameterConfig[];
  constraints_groups: ConstraintGroupConfig[];
  objectives_groups: ObjectiveGroupConfig[];
  children: any[];
}

export interface ConstraintGroupConfig {
  type: string;
  constraints: UnitConstraintConfig[];
}

export interface ObjectiveGroupConfig {
  type: string;
  objectives: UnitObjectiveConfig[];
  disjoint_objectives: DisjointObjectiveConfig[];
}

export interface UnitParameterConfig {
  name: string;
  type: string;
  tooltip: string;
  label: string;
  required: boolean;
  default: string | number | null;
}

export interface UnitConstraintConfig {
  constraint_name: string;
  label: string;
  tooltip: string;
  parameters: UnitParameterConfig[];
}

export interface UnitObjectiveConfig {
  name: string;
  label: string;
  tooltip: string;
  parameters: UnitParameterConfig[];
}

export interface DisjointObjectiveConfig {
  label: string;
  objectives: UnitObjectiveConfig[];
  tooltip: string;
}
