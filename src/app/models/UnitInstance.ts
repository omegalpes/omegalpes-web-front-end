export interface UnitInstance {
  id: string;
  className: string;
  variableName?: string;
  properties: UnitInstanceParameter[];
  objectives: UnitInstanceObjective[];
  constraints: UnitInstanceConstraint[];
  children?: UnitInstance[];
  linksUnits: string[];
  exportsToNode: string[];
  types?: string[];
}

export interface UnitInstanceParameter {
  name: string;
  type: string;
  value: string | number | null;
}

export interface UnitInstanceConstraint {
  name: string;
  properties: UnitInstanceParameter[];
}

export interface UnitInstanceObjective {
  name: string;
  weight?: number;
  properties?: UnitInstanceParameter[];
}
