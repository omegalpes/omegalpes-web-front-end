export enum Subtype {
  UNSPECIFIED = 'unspecified',
  FIXED = 'fixed',
  VARIABLE = 'variable',
  STORAGE = 'storage',
  CONVERSION = 'conversion',
  NODE = 'node',
  SINGLE = 'single',
  REVERSIBLE = 'reversible'
}
