import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { CanvasState } from './stores/canvas/canvas.state';
import { ModelObjectsState } from './stores/model-objects/model-objects.state';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarModule } from './features/toolbar/toolbar.component';
import { BoardModule } from './features/board/board.component';
import { CanvasService } from './services/canvas/canvas.service';
import { IconsService } from './services/graphics/icons/icons.service';
import { GraphicsService } from './services/graphics/graphics.service';
import { HttpClientModule } from '@angular/common/http';
import { SpriteModule } from './commons/sprite/sprite.component';
import { ConfigService } from './services/config/config.service';
import { SelectedUnitState } from './stores/selected-unit/selected-unit.state';
import { LinkResolverService } from './services/link-resolver/link-resolver.service';
import { IoService } from './services/io/io.service';
import { ResultModule } from './features/result/result.component';
import { CodeEditorModule } from './features/code-editor/code-editor.component';
import { SettingsModule } from './features/settings/settings.component';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxsModule.forRoot([CanvasState, SelectedUnitState, ModelObjectsState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    BoardModule,
    ToolbarModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SettingsModule,
    ResultModule,
    CodeEditorModule,
    SpriteModule
  ],
  providers: [
    CanvasService,
    IconsService,
    GraphicsService,
    ConfigService,
    LinkResolverService,
    IoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
