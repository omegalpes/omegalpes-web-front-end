import { Component, NgModule, Inject, Input } from '@angular/core';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { Shape } from 'src/app/stores/canvas/canvas.state';

@Component({
  selector: 'app-dialog-remove',
  templateUrl: './dialog-remove.component.html',
  styleUrls: ['./dialog-remove.component.scss']
})
export class DialogRemoveComponent {
  @Input() messageToDisplay: string;

  constructor(
    public dialogRef: MatDialogRef<DialogRemoveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  onOkClick(): void {
    this.dialogRef.close(true);
  }
}

export interface DialogData {
  shape: Shape;
}

@NgModule({
  imports: [CommonModule, MatDialogModule, MatButtonModule],
  entryComponents: [DialogRemoveComponent],
  declarations: [DialogRemoveComponent],
  exports: [DialogRemoveComponent]
})
export class DialogRemoveModule {}
