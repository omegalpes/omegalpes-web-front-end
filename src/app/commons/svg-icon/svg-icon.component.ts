import {
  Component,
  NgModule,
  ChangeDetectionStrategy,
  Input
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SvgIconComponent {
  @Input() icon: string;

  @Input() label = '';

  @Input() classes = '';

  @Input() width: string;

  @Input() height: string;

  @Input() viewBox: string;

  @Input() preserveAspectRatio = 'xMinYMax meet';
}

@NgModule({
  imports: [CommonModule],
  declarations: [SvgIconComponent],
  exports: [SvgIconComponent]
})
export class SvgIconModule {}
