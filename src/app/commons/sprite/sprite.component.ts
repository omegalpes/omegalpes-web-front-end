import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-sprite',
  templateUrl: './sprite.component.html'
})
export class SpriteComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [SpriteComponent],
  exports: [SpriteComponent]
})
export class SpriteModule {}
