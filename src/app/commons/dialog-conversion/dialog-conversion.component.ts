import {Component, Inject, Input, NgModule, OnInit} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatCardModule,
  MatDialogModule,
  MatDialogRef,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatRadioModule,
  MatSelectModule
} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfigService} from 'src/app/services/config/config.service';
import {EnergyConfig} from 'src/app/models/Config';
import {Subtype} from 'src/app/models/Subtype';
import {GraphicsService} from 'src/app/services/graphics/graphics.service';

@Component({
  selector: 'app-dialog-conversion',
  templateUrl: './dialog-conversion.component.html',
  styleUrls: ['./dialog-conversion.component.scss']
})
export class DialogConversionComponent implements OnInit {
  @Input() messageToDisplay: string;
  DEFAULT_ENERGY: EnergyConfig = {
    energy: 'Electrical',
    color: '#7030a0'
  };

  energies: EnergyConfig[] = [];
  productionSelected = {
    type: 'FixedProductionUnit',
    energyType: this.DEFAULT_ENERGY
  };
  consumptionSelected = {
    type: 'FixedConsumptionUnit',
    energyType: this.DEFAULT_ENERGY
  };
  secondConsumptionSelected = {
    type: 'FixedConsumptionUnit',
    energyType: this.DEFAULT_ENERGY
  };
  consumptionToProductionEnter: string;
  powerOutputEnter: string;
  coefficientOfPerformanceEnter: string;
  lossesAsPercentageEnter: string;

  constructor(
    public dialogRef: MatDialogRef<DialogConversionComponent>,
    public readonly configService: ConfigService,
    private readonly graphicService: GraphicsService,
    @Inject(MAT_DIALOG_DATA) public data: {
      unitConfig: Subtype,
      title: string
    }
  ) {}

  ngOnInit() {
    this.energies = this.configService.config.model.energies;
    document.documentElement.style.setProperty(
      '--colorPropertyProduction', ''
    );
    document.documentElement.style.setProperty(
      '--colorPropertyConsumption', ''
    );
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  onOkClick(): void {
    if (this.data.unitConfig === Subtype.SINGLE) {
      this.dialogRef.close({
       productionUnit: this.productionSelected,
       consumptionUnit: this.consumptionSelected,
       consumptionToProduction : this.consumptionToProductionEnter
      });
    }
    // Heatpump case
    if (this.data.unitConfig === Subtype.CONVERSION) {
      this.productionSelected.energyType =
        this.configService.config.model.energies.find(energy => energy.energy === 'Thermal');
      this.consumptionSelected.energyType =
        this.configService.config.model.energies.find(energy => energy.energy === 'Thermal');
      this.secondConsumptionSelected.energyType =
        this.configService.config.model.energies.find(energy => energy.energy === 'Electrical');
      this.dialogRef.close({
        productionUnit: this.productionSelected,
        consumptionUnit: this.consumptionSelected,
        secondConsumptionUnit: this.secondConsumptionSelected,
        powerOutput: this.powerOutputEnter,
        coefficientOfPerformance: this.coefficientOfPerformanceEnter,
        lossesAsPercentage: this.lossesAsPercentageEnter
      });
    }
  }

  isHeatPump() {
    return this.data.unitConfig === Subtype.CONVERSION;
  }

  public changeColorProduction() {
    document.documentElement.style.setProperty(
      '--colorPropertyProduction',
      this.productionSelected.energyType.color
    );
    document.documentElement.style.setProperty(
      '--colorTextProduction',
      this.graphicService.getContrastColor(this.productionSelected.energyType.color)
    );
  }

  public changeColorConsumption() {
    document.documentElement.style.setProperty(
      '--colorPropertyConsumption',
      this.consumptionSelected.energyType.color
    );
    document.documentElement.style.setProperty(
      '--colorTextConsumption',
      this.graphicService.getContrastColor(this.consumptionSelected.energyType.color)
    );
  }
}

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatOptionModule,
    FormsModule,
    MatRadioModule,
    ReactiveFormsModule
  ],
  entryComponents: [DialogConversionComponent],
  declarations: [DialogConversionComponent],
  exports: [DialogConversionComponent]
})
export class DialogConversionModule {}
