import { Component, Input, NgModule, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CanvasService } from '../../services/canvas/canvas.service';

@Component({
  selector: 'app-lateral-panel',
  templateUrl: './lateral-panel.component.html',
  styleUrls: ['./lateral-panel.component.scss']
})
export class LateralPanelComponent {
  @Input() isVisible: boolean;
  @Input() isRightPanel: boolean;
  @Input() top?: string;
  @Input() right?: string;
  @Input() bottom?: string;
  @Input() left?: string;

  constructor(private readonly canvasService: CanvasService) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnChanges(changes: SimpleChanges) {
    if (this.isRightPanel) {
      if (changes.isVisible.currentValue) {
        this.canvasService.disableZoom();
      } else {
        this.canvasService.enableZoom();
      }
    }
  }
}

@NgModule({
  imports: [CommonModule],
  declarations: [LateralPanelComponent],
  exports: [LateralPanelComponent]
})
export class LateralPanelModule {}
