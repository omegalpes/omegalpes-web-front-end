import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapeButtonComponent } from './shape-button.component';

describe('ShapeButtonComponent', () => {
  let component: ShapeButtonComponent;
  let fixture: ComponentFixture<ShapeButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShapeButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShapeButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
