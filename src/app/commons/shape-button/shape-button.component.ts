import {
  Component,
  NgModule,
  Input,
  Output,
  EventEmitter,
  OnInit
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { SvgIconModule } from '../svg-icon/svg-icon.component';
import { UnitConfig } from 'src/app/models/Config';
import { EnergyType } from 'src/app/models/EnergyType';

@Component({
  selector: 'app-shape-button',
  templateUrl: './shape-button.component.html',
  styleUrls: ['./shape-button.component.scss']
})
export class ShapeButtonComponent implements OnInit {
  @Input() unit: UnitConfig;
  @Input() groupName: string;
  @Output() buttonClicked = new EventEmitter<UnitConfig>();
  isInputUnit: boolean;

  ngOnInit(): void {
    this.isInputUnit = !this.unit.types.find(
      type => type.toLowerCase() === ( EnergyType.PRODUCTION.toLowerCase() )
    );
  }
}

@NgModule({
  imports: [CommonModule, MatButtonModule, SvgIconModule ],
  declarations: [ShapeButtonComponent],
  exports: [ShapeButtonComponent]
})
export class ShapeButtonModule {}
