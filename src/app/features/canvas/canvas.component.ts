import { Component, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Shape, CanvasState } from 'src/app/stores/canvas/canvas.state';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CanvasService } from 'src/app/services/canvas/canvas.service';
import { MatButtonModule } from '@angular/material/button';
import { ImportFileModel, IoService } from 'src/app/services/io/io.service';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements AfterViewInit {
  @Output() displayShapesMenu = new EventEmitter<void>();

  @Select(CanvasState.shapes) shapes$: Observable<Shape[]>;

  constructor(
    private readonly canvasService: CanvasService,
    private readonly store: Store,
    private readonly ioService: IoService) {}

  ngAfterViewInit(): void {
    if (localStorage.getItem('model') !== '') {
      const model: ImportFileModel = JSON.parse(
        localStorage.getItem('model')
      );
      this.ioService.loadCanvas(model);
    } else {
      this.canvasService.setCanvasElement();
    }
  }
}

@NgModule({
  imports: [CommonModule, MatButtonModule],
  declarations: [CanvasComponent],
  exports: [CanvasComponent]
})
export class CanvasModule {}
