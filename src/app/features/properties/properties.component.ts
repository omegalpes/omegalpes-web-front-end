import { Component, NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { Shape } from 'src/app/stores/canvas/canvas.state';
import { Select, Store } from '@ngxs/store';
import { SelectedUnitState } from 'src/app/stores/selected-unit/selected-unit.state';
import { Observable } from 'rxjs';
import { MatIconModule } from '@angular/material';
import { RemoveSelectedUnit } from 'src/app/stores/selected-unit/selected-unit.actions';
import { PropertiesFormModule } from 'src/app/domains/properties-form/properties-form.component';



@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent {
  @Select(SelectedUnitState.shape) shape$: Observable<Shape>;

  constructor(public store: Store) {}

  public close(): void {
    this.store.dispatch(new RemoveSelectedUnit());
  }
}

@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    PropertiesFormModule
  ],
  declarations: [PropertiesComponent],
  exports: [PropertiesComponent]
})
export class PropertiesModule {}
