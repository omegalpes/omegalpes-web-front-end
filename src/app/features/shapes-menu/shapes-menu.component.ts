import {
  Component,
  NgModule,
  EventEmitter,
  Output,
  Input
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { CanvasService } from 'src/app/services/canvas/canvas.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { ShapeButtonModule } from 'src/app/commons/shape-button/shape-button.component';
import { UnitGroupsConfig, UnitConfig } from 'src/app/models/Config';
import { ConfigService } from 'src/app/services/config/config.service';
import { UnitInstancesService } from 'src/app/services/unit-instances/unit-instances.service';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { DialogConversionComponent, DialogConversionModule } from 'src/app/commons/dialog-conversion/dialog-conversion.component';
import { Subtype } from 'src/app/models/Subtype';
import _ from 'lodash';

@Component({
  selector: 'app-shapes-menu',
  templateUrl: './shapes-menu.component.html',
  styleUrls: ['./shapes-menu.component.scss']
})
export class ShapesMenuComponent {
  @Input() isVisible: boolean;
  @Output() openCloseShapesMenu = new EventEmitter<void>();
  unitGroups: UnitGroupsConfig[];

  constructor(
    private readonly canvasService: CanvasService,
    private readonly configService: ConfigService,
    private readonly unitInstancesService: UnitInstancesService,
    private readonly dialog: MatDialog
  ) {
    this.configService
      .getUnitGroupsConfig()
      .subscribe(unitGroups => (this.unitGroups = unitGroups));
  }

  public createUnitInstance(unitConfig: UnitConfig) {
    if (unitConfig['subtype'] === Subtype.SINGLE || unitConfig['subtype'] === Subtype.CONVERSION) {
      const dialogRef = this.dialog.open(DialogConversionComponent, {
        width: '28rem',
        height: '28rem',
        data: {
          unitConfig: unitConfig['subtype'],
          title: 'Choice of unit conversion'
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (unitConfig['subtype'] === Subtype.SINGLE && result) {
          unitConfig.children.length = 0;
          unitConfig.children.push(result.consumptionUnit);
          unitConfig.children.push(result.productionUnit);
          const unitInstance = this.unitInstancesService.createUnitInstance(
            unitConfig
          );
          unitInstance.properties.find(p => p.name === 'energy_type_in').value = result.consumptionUnit.energyType.energy;
          unitInstance.properties.find(p => p.name === 'energy_type_out').value = result.productionUnit.energyType.energy;
          unitInstance.properties.find(p => p.name === 'efficiency_ratio').value = result.consumptionToProduction;
          this.canvasService.addShape({unitInstance});
        }
        if (unitConfig['subtype'] === Subtype.CONVERSION && result) {
          unitConfig.children.length = 0;
          unitConfig.children.push(result.consumptionUnit);
          unitConfig.children.push(result.secondConsumptionUnit);
          unitConfig.children.push(result.productionUnit);
          const unitInstance = this.unitInstancesService.createUnitInstance(
            unitConfig
          );
          unitInstance.properties.find(p => p.name === 'p_out_therm').value = result.powerOutput;
          unitInstance.properties.find(p => p.name === 'cop').value = result.coefficientOfPerformance;
          unitInstance.properties.find(p => p.name === 'losses').value = result.lossesAsPercentage;
          this.canvasService.addShape({unitInstance});
        }
        this.dialog.closeAll();
      });
    } else {
      if (_.get(unitConfig, 'children', []).length > 0) {
        unitConfig.children = unitConfig.children.map(unit => ({
          type: unit
        }));
      }
      const unitInstance = this.unitInstancesService.createUnitInstance(
        unitConfig
      );
      this.canvasService.addShape({
        unitInstance
      });
    }
  }
}

@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule,
    ShapeButtonModule,
    MatButtonModule,
    MatIconModule,
    DialogConversionModule
  ],
  declarations: [ShapesMenuComponent],
  exports: [ShapesMenuComponent]
})
export class ShapesMenuModule {}
