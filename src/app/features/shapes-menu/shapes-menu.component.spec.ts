import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapesMenuComponent } from './shapes-menu.component';

describe('ShapesMenuComponent', () => {
  let component: ShapesMenuComponent;
  let fixture: ComponentFixture<ShapesMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShapesMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShapesMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
