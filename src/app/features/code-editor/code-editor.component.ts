import { Component, ElementRef, NgModule, OnInit, ViewChild } from '@angular/core';
import * as ace from 'ace-builds';
import 'ace-builds/src-noconflict/mode-python';
import 'ace-builds/src-noconflict/theme-github';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/ext-beautify';
import { MatButtonModule, MatIconModule, MatToolbarModule } from '@angular/material';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { AppComponent } from 'src/app/app.component';
import { ModelParameterConfig } from 'src/app/models/Config';
import { ToasterService } from 'src/app/services/toaster/toaster.service';
import { IoService } from 'src/app/services/io/io.service';
import { ModelObjectsState } from 'src/app/stores/model-objects/model-objects.state';

const THEME = 'ace/theme/github';
const LANG = 'ace/mode/python';

@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.scss']
})

export class CodeEditorComponent {
  private editorBeautify;
  @ViewChild('editor', {static: false}) private editor: ElementRef<HTMLElement>;

  aceEditor;
  modelParametersConfig: ModelParameterConfig[] = [];  fileUrl;
  data: string = null;
  saver = false;

  constructor(
    private appComponent: AppComponent,
    private sanitizer: DomSanitizer,
    private toasterService: ToasterService,
    private ioService: IoService
    ) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit(): void {
    ace.config.set('fontSize', '14px');
    ace.config.set('basePath', 'https://unpkg.com/ace-builds@1.4.12/src-noconflict');
    this.editorBeautify = ace.require('ace/ext/beautify');
    const editorOptions = this.getEditorOptions();
    this.aceEditor = ace.edit(this.editor.nativeElement, editorOptions);
    this.aceEditor.session.setMode(LANG);
    this.aceEditor.setTheme(THEME);
    this.aceEditor.setShowFoldWidgets(true);
    this.beautifyContent();
    this.aceEditor.session.setValue(this.ioService.getScript());
  }

  public setEditor(script: string) {
    const aceEditor = ace.edit(this.editor.nativeElement);
    aceEditor.session.setValue(script);
  }

  public getValue() {
    return this.aceEditor.getValue();
  }

  public getEditorOptions(): Partial<ace.Ace.EditorOptions> & { enableBasicAutocompletion?: boolean; } {
    const extraEditorOptions = {
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: true,
      enableSnippets: true,
    };
    const margedOptions = Object.assign(extraEditorOptions);
    return margedOptions;
  }

  public beautifyContent() {
    if (this.aceEditor && this.editorBeautify) {
      const session = this.aceEditor.getSession();
      this.editorBeautify.beautify(session);
    }
  }

  public getCodeEditor() {
    return this.appComponent.getCodeEditor();
  }

  public setBoard() {
    this.appComponent.setBoard();
  }

  public save() {
      localStorage.setItem('script', this.getValue());
      const data = this.ioService.getScript();
      const blob = new Blob([data], { type: 'application/octet-stream' });
      this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
      this.toasterService.openToast(
        'Votre script a bien été sauvegardé.',
        'Ok'
      );
      this.saver = true;
  }

  public getSave() {
    return this.saver;
  }

  public async executeScript() {
      localStorage.setItem('error', '');
      await this.ioService.executeScript();
      let error = localStorage.getItem('error');
      console.log(error);
      if (error !== '') {
        error = error.charAt(1).toUpperCase() + error.substring(2, error.length - 1);
        this.toasterService.openToast(
          error,
          'Ok',
        );
      } else {
        const result = JSON.parse(localStorage.getItem('result'));
        if (result.optimisation_result === 'INFEASIBLE') {
          this.toasterService.openToast(
            'Sorry, the optimisation problem has no feasible solution !',
            'Ok',
          );
        } else {
          this.appComponent.setResult();
        }
      }
  }
}

@NgModule({
  providers: [],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    BrowserModule
  ],
  declarations: [CodeEditorComponent],
  exports: [CodeEditorComponent]
})

export class CodeEditorModule {}
