import { KeyValue } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, NgModule, OnInit, ViewChild } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import { BrowserModule } from '@angular/platform-browser';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';
import { AppComponent } from 'src/app/app.component';
import { MatButtonModule, MatIconModule, MatToolbarModule } from '@angular/material';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  private imageToShow = [];
  myURL: any;

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  private units = [];
  private plots = [];
  private nodes = [];
  private heatPumps = [];
  private unitsProduction = [];
  private unitsStorage = [];
  private unitsConsumption = [];
  private image = [];

  private generalData = [
    {
      Name: 'Date de début ',
      Value: '',
      IsChecked: true,
    },
    {
      Name: 'Date de fin ',
      Value: '',
      IsChecked: true,
    },
    {
      Name: 'Pas de temps ',
      Value: '',
      IsChecked: true,
    },
    {
      Name: 'Durée de l\'optimisation ',
      Value: '',
      IsChecked: true,
    }
  ];

  constructor(private readonly http: HttpClient, private appComponent: AppComponent) {}


  ngOnInit() {
    this.setUnits();
    this.setGeneralData();
    this.setPlots();
    this.setNodes();
    this.setHeatPump();
  }

  compare(string1, string2){
    return string1 === string2;
  }

  getGeneralData() {
    for (const element of this.generalData) {
      if (element.IsChecked) {
        return true;
      }
    }
    return false;
  }

  setGeneralData() {
    const value = JSON.parse(localStorage.getItem('result'));
    this.generalData[0].Value = value.startDate;
    this.generalData[1].Value = value.endDate;
    this.generalData[2].Value = value.timeStep;
    this.generalData[3].Value = value.optimisationTime;
  }


  setUnits() {
    const value = JSON.parse(localStorage.getItem('result'));
    value.units.forEach(element => {
      element.IsChecked = false;
      let res : string = element.variableName.name;
      element.variableName.name = res.charAt(0).toUpperCase() + res.replace('_',' ').substring(1,res.length);
      if (element.variableType.name.includes('Production')) {        
        this.unitsProduction.push(element);
      }
      if (element.variableType.name.includes('Consumption')) {
        this.unitsConsumption.push(element);
      }
      if (element.variableType.name.includes('Storage')) {
        this.unitsStorage.push(element);
      }
      if(element.variableType.name.includes('HeatPump')){}
      else   
        this.units.push(element);
    });
  }

  setHeatPump(){
    const value = JSON.parse(localStorage.getItem('result'));
    value.units.forEach(element => {
      if(element.variableType.name.includes('HeatPump')){
        for(let i = 0; i < element.children.length; i++){
          element.children[i].IsChecked = false;
        }
        this.heatPumps.push(element);
      }
    });
  }

  setNodes() {
    const value = JSON.parse(localStorage.getItem('result'));
    value.nodes.forEach(element => {
      let res : string = element.nodeName;
      element.nodeName = res.charAt(0).toUpperCase() + res.replace('_',' ').substring(1,res.length);
      element.IsChecked = false;
      this.nodes.push(element);
    });
  }

  getProfilByUnit(unit){
    for(let i = 0; i< this.plots.length; i++){
      if(this.plots[i].objectName.includes(unit.variableName.name)){
        return true;
      }
    }
    return false;
  }

  getNodes(){
    return this.nodes.length > 0;
  }

  getUnitsProduction() {
    return this.unitsProduction.length > 0;
  }

  getUnitsConsumption() {
    return this.unitsConsumption.length > 0;
  }

  getUnitsStorage() {
    return this.unitsStorage.length > 0;
  }

  getPlotIsChecked(){
    for(let i = 0; i < this.plots.length; i++){
      if(this.plots[i].IsChecked) 
        return true;
    }
    return false;
  }

  setPlots() {
    const value = JSON.parse(localStorage.getItem('result'));
    value.plots.forEach(element => {      
      let res : string = element.objectName;
      element.objectName = res.charAt(0).toUpperCase() + res.replace('_',' ').substring(1,res.length);
      element.IsChecked = false;
      this.plots.push(element);
    });
    let counter = 0;
    value.plots.forEach(element => {
      this.onURLinserted(element.plotName, counter);
      counter++;
    });
  }

  private onCompare(_left: KeyValue<any, any>, _right: KeyValue<any, any>): number {
    return -1;
  }

  onURLinserted(imageUrl: string, counter: number) {
      this.getImage(imageUrl).subscribe(data => {
        this.createImageFromBlob(data, counter);
      }, error => {});
  }

  getImage(imageUrl: string): Observable<Blob> {
    const url = imageUrl.substring(6, imageUrl.length);
    return this.http.get(`${environment.omegalpesWebBackEnd}/send_plot?name=` + url, { responseType: 'blob' });
  }


  createImageFromBlob(image: Blob, counter: number) {
    const reader = new FileReader();
    if (image) {
      reader.readAsDataURL(image);
   }
    reader.addEventListener('load', () => {
       this.imageToShow[counter] = reader.result;
    }, false);
  }

  getBooleanProduction() {
    for(let i = 0; i < this.unitsProduction.length; i++) {
      if (this.unitsProduction[i].IsChecked === true) {
        return true;
      }
    }
    return false;
  }

  getBooleanConsumption() {
    for(let i = 0; i < this.unitsConsumption.length; i++) {
      if (this.unitsConsumption[i].IsChecked === true) {
        return true;
      }
    }
    return false;
  }

  getBooleanStorage() {
    for(let i = 0; i < this.unitsStorage.length; i++) {
      if (this.unitsStorage[i].IsChecked === true) {
        return true;
      }
    }
    return false;
  }

  getBooleanHeatPump() {
    for(let i = 0; i < this.heatPumps.length; i++) {
      for(let j = 0; j < this.heatPumps[i].children.length; j++) {
        if (this.heatPumps[i].children[j].IsChecked === true) {
          return true;
        }
      }
    }
    return false;
  }

  getBooleanNode() {
    for (let i = 0; i < this.unitsStorage.length; i++) {
      if (this.nodes[i].IsChecked === true) {
        return true;
      }
    }
    return false;
  }

  public downloadAsPDF() {
    const doc = new jsPDF();
    const img = new Image();
    const pdfTable = this.pdfTable.nativeElement;
    const html1 = htmlToPdfmake(pdfTable.innerHTML.replace('h4', 'h6'));
    let image = []; 
    let today = new Date();   
    var dd = String(today.getDate()).padStart(2, '0');
    var currentMonth: string[] = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre", ];  
    var mm = currentMonth[today.getMonth()];
    var yyyy = today.getFullYear();
    let currentDate = dd+ ' ' + mm + ' '  + yyyy;

    var title = htmlToPdfmake("<h4 style='text-align: center'>Optimisation des résultats</h4><hr /><br>");
    var titleDate = htmlToPdfmake("<div style='clear: both'><h6 style='float: left'>OMEGALPES</h6><h6 style='float: right'>" + currentDate + "</h6></div><br><hr />");
    const documentDefinition = { content: []};
    documentDefinition.content.push(titleDate);
    documentDefinition.content.push(title);
    documentDefinition.content.push(html1);
    var stringPlot = htmlToPdfmake("<br><h6><u>Affichage de la liste des plots</u></h6><br>");
    documentDefinition.content.push(stringPlot);
    this.imageToShow.forEach(element => {
      documentDefinition.content.push({image:  element, width: 500, height: 350});
    });
    const html2 = htmlToPdfmake(image);    
    pdfMake.createPdf(documentDefinition).download('OMEGALPES - Résultat du ' + currentDate + '.pdf');
  }

  public getResult() {
    return this.appComponent.getResult();
  }

  public setBoard() {
    this.appComponent.setBoard();
  }
}

@NgModule({
  providers: [],
  imports: [
    MatCheckboxModule,
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    BrowserModule,
  ],
  declarations: [ResultComponent],
  exports: [ResultComponent]
})

export class ResultModule {}



