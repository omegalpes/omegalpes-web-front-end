import { Component, NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatSnackBarModule,
  MatDialog,
  MatDialogModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { ToasterService } from 'src/app/services/toaster/toaster.service';
import { Store } from '@ngxs/store';
import { ConfigService } from 'src/app/services/config/config.service';
import { ModelParameterConfig } from 'src/app/models/Config';
import {
  DialogModelParametersComponent,
  DialogModelParametersModule
} from 'src/app/domains/dialog-model-parameters/dialog-model-parameters.component';
import { AddModelParameterConfig } from 'src/app/stores/model-objects/model-objects.actions';
import { ModelObjectsState } from 'src/app/stores/model-objects/model-objects.state';
import { Subtype } from 'src/app/models/Subtype';
import { IoService } from 'src/app/services/io/io.service';
import { DialogRemoveComponent } from 'src/app/commons/dialog-remove/dialog-remove.component';
import { CanvasService } from 'src/app/services/canvas/canvas.service';
import { AppComponent } from 'src/app/app.component';
import {MatTooltipModule} from "@angular/material/tooltip";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
  modelParametersConfig: ModelParameterConfig[] = [];
  data: string;
  edit: boolean;
  compil: boolean;

  constructor(
    private readonly toasterService: ToasterService,
    private readonly matDialog: MatDialog,
    private readonly store: Store,
    private readonly configService: ConfigService,
    private readonly ioService: IoService,
    private readonly canvasService: CanvasService,
    private appComponent: AppComponent,
  ) {
    this.configService
      .getModelConfig()
      .subscribe(model => (this.modelParametersConfig = model.parameters));
  }

  public openFileClick() {
    this.ioService.importModel();
  }

  public centerView() {
    this.canvasService.resetPositionAndScale();
  }

  public getBoard() {
    return this.appComponent.getBoard();
  }

  public getResult() {
    return this.appComponent.getResult();
  }

  public getCodeEditor() {
    return this.appComponent.getCodeEditor();
  }

  public setBoard() {
    this.appComponent.setBoard();
  }

  public setCodeEditor() {
    this.appComponent.setCodeEditor();
  }

  public async setResult() {
    await this.ioService.saveModel();
    this.appComponent.setResult();
  }

  public async setSettings() {
    await this.ioService.saveModel();
    this.appComponent.setSettings();
  }


  public async openGlobalProperties(runModelNext = false, executeModelNext= false) {
    const modelParametersConfig = this.modelParametersConfig;
    const dialogRef = this.matDialog.open(DialogModelParametersComponent, {
      width: '16rem',
      data: modelParametersConfig
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!result) {
        return;
      }
      modelParametersConfig.forEach(modelParameter => {
        this.store.dispatch(
          new AddModelParameterConfig({
            name: modelParameter.name,
            type: modelParameter.type,
            value: modelParameter.value
          })
        );
      });
      if (runModelNext) {
        if (this.compil) {
          this.generate();
        } else {
          this.generateScript();
        }
      }
      if (executeModelNext) {
        await this.ioService.execute(this.getModelToSend());
        this.loadResult();
      }
    });
  }

  public saveFile() {
    this.ioService.exportModel();
  }

  public cleanCanvas() {
    const dialogRef = this.matDialog.open(DialogRemoveComponent, {
      width: '16rem',
      data: 'Would you clear the board ?'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ioService.cleanStoresAndCanvas();
      }
    });
  }

  public verifyModel() {
    this.ioService.saveModel();
    this.compil = true;
    this.edit = false;
    if (!this.allUnitsNamesAreUnique()) {
      this.toasterService.openToast(
        'All units names must be unique, some of them have the same name. Please, rename your units to avoid duplication.',
        'Ok',
      );
      return;
    }

    if (this.store.selectSnapshot(ModelObjectsState.modelParametersConfig).length === 0) {
      this.openGlobalProperties(true);
      this.toasterService.openToast(
        'Global parameters are missing, fill them to run this model.',
        'Ok'
      );
      return;
    }
  }

  public async execute() {
    this.ioService.saveModel();
    this.compil = true;
    this.edit = false;
    if (!this.allUnitsNamesAreUnique()) {
      this.toasterService.openToast(
        'All units names must be unique, some of them have the same name. Please, rename your units to avoid duplication.',
        'Ok',
      );
      return;
    }

    if (this.store.selectSnapshot(ModelObjectsState.modelParametersConfig).length === 0) {
      await this.openGlobalProperties(false, true);
      this.toasterService.openToast(
        'Global parameters are missing, fill them to run this model.',
        'Ok'
      );
      return;
    } else {
      await this.ioService.execute(this.getModelToSend());
    }
    this.loadResult();
  }

  public loadResult() {
    const res = localStorage.getItem('result');
    if (res !== '') {
      const result = JSON.parse(res);
      if (result.optimisation_result === 'INFEASIBLE') {
        this.toasterService.openToast(
          'Sorry, the optimisation problem has no feasible solution !',
          'Ok',
        );
      } else {
        this.appComponent.setResult();
      }
    } else {
      let error = localStorage.getItem('error');
      if (error !== '') {
        error = error.charAt(1).toUpperCase() + error.substring(2, error.length - 1);
        this.toasterService.openToast(
          'Ce paramètre est nécessaire : ' + error,
          'Ok',
        );
      }
    }
  }

  public generate() {
    this.verifyModel();
    this.ioService.generateModel(this.getModelToSend());
  }

  public async generateScript() {
    await this.ioService.saveModel();
    this.compil = false;
    this.edit = true;
    if (!this.allUnitsNamesAreUnique()) {
      this.toasterService.openToast(
        'All units names must be unique, some of them have the same name. Please, rename your units to avoid duplication.',
        'Ok',
      );
      return;
    }

    if (
      this.store.selectSnapshot(ModelObjectsState.modelParametersConfig)
        .length === 0
    ) {
      this.openGlobalProperties(true);

      this.toasterService.openToast(
        'Global parameters are missing, fill them to run this model.',
        'Ok'
      );
      return;
    }
    await this.ioService.generateScript(this.getModelToSend());
    this.setCodeEditor();
  }

  private allUnitsNamesAreUnique(): boolean {
    const unitsInstances = this.store.selectSnapshot(
      ModelObjectsState.unitsInstance
    );

    const namesWithoutDuplication = new Set(
      unitsInstances.map(
        unitInstance =>
          unitInstance.properties.find(property => property.name === 'name')
            .value
      )
    );
    return unitsInstances.length === namesWithoutDuplication.size;
  }

  private getModelToSend(): object {
    const model = this.store.selectSnapshot(ModelObjectsState.modelToSend);
    model.unitsInstance.forEach(
      unitInstance =>
        (unitInstance.variableName = unitInstance.properties
          .find(parameter => parameter.name === 'name')
          .value.toString()
          .toLowerCase()
          .replace(' ', '_'))
    );

    const unitsInstanceWithChildDuplicate = model.unitsInstance.filter(
      unitInstance =>
        this.configService.findUnitConfigByClassname(unitInstance.className)
          .subtype !== Subtype.NODE
    );

    const childrenIds = [];
    model.unitsInstance.forEach(unitInstance => {
      if (unitInstance.children) {
       unitInstance.children.forEach(child => childrenIds.push(child.id));
      }
    });
    const unitsInstance = unitsInstanceWithChildDuplicate.filter(
      unit => !childrenIds.includes(unit.id)
    );
    const nodesInstance = model.unitsInstance.filter(
      unitInstance =>
        this.configService.findUnitConfigByClassname(unitInstance.className)
          .subtype === Subtype.NODE
    );

    return {
      modelParametersConfig: {
        properties: model.modelParametersConfig.map(modelParameter => ({
          name: modelParameter.name,
          type: modelParameter.type,
          value: modelParameter.value
        }))
      },
      unitsInstance,
      nodesInstance
    };
  }

  public exportImage() {
    this.ioService.exportAsImage();
  }
}

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatDialogModule,
    DialogModelParametersModule,
    MatTooltipModule
  ],
  declarations: [ToolbarComponent],
  exports: [ToolbarComponent]
})
export class ToolbarModule {}
