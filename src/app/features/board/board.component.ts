import {AfterViewChecked, AfterViewInit, Component, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { CanvasModule } from '../canvas/canvas.component';
import { ShapesMenuModule } from '../shapes-menu/shapes-menu.component';
import { PropertiesModule } from '../properties/properties.component';
import { LateralPanelModule } from '../../commons/lateral-panel/lateral-panel.component';
import { Select } from '@ngxs/store';
import { SelectedUnitState } from 'src/app/stores/selected-unit/selected-unit.state';
import { Observable } from 'rxjs';
import { Shape } from 'src/app/stores/canvas/canvas.state';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements AfterViewChecked {
  @Select(SelectedUnitState.shape) isPropertiesVisible$: Observable<Shape>;

  constructor(private cdRef: ChangeDetectorRef) {}
  isShapesMenuVisible = true;

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
}

@NgModule({
  imports: [
    CommonModule,
    LateralPanelModule,
    CanvasModule,
    ShapesMenuModule,
    PropertiesModule
  ],
  declarations: [BoardComponent],
  exports: [BoardComponent]
})
export class BoardModule {}
