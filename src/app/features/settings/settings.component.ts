import { Component, NgModule, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatExpansionModule, MatIconModule, MatRadioModule, MatToolbarModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { Console } from 'console';
import { element } from 'protractor';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit {
  unitSelectedIndex: number;
  unitSelectedProperties = [];
  unitSelectedConstraints = [];
  unitSelectedObjectives = [];
  unitSelectedNodes = [];
  unit: any;
  unitName = [];
  constructor(private appComponent: AppComponent) {}

  ngOnInit() {
    this.unit = this.getUnit();
    this.getUnitName();
  }

  public setBoard() {
    this.appComponent.setBoard();
  }

  public getUnit() {
    const model = localStorage.getItem('model');
    if (model !== null) {
      const units: Model = JSON.parse(localStorage.getItem('model'));
      const result = units.modelObjectsStore.unitsInstance;
      return result;
    }
    return null;
  }

  public getUnitName() {
    this.unit.forEach(element => {
        this.unitName.push(element.properties[0].value);
    });
  }

  public getUnitSelected() {
    return this.unitSelectedIndex;
  }

  public getPropertiesUnit() {
    const result = [];
    this.unitSelectedProperties = [];
    this.unitSelectedConstraints = [];
    this.unitSelectedObjectives = [];
    this.unitSelectedNodes = [];
    this.unit.forEach(element => {
        result.push(element);
    });
    this.unitSelectedProperties = result[this.unitSelectedIndex].properties;
    if (result[this.unitSelectedIndex].className !== 'EnergyNode') {
      this.unitSelectedConstraints = result[this.unitSelectedIndex].constraints;
      this.unitSelectedObjectives = result[this.unitSelectedIndex].objectives;
    } else {
      result[this.unitSelectedIndex].linksUnits.forEach(element => {
        const result = this.getUnitNameById(element);
        this.unitSelectedNodes.push(result);
      });
    }
  }

  public getUnitNameById(id) {
    for (let i = 0; i < this.unit.length; i++) {    
      if (this.unit[i].id === id) {
        return this.unit[i].properties[0].value;
      }
      if(this.unit[i].children.length > 0){
        for(let j = 0; j < this.unit[i].children.length; j++){
          if (this.unit[i].children[j].id === id) {
            return this.unit[i].children[j].properties[0].value;
          }
        }
      }      
    }
  }

  public isEmpty(tab: []) {
    return tab.length > 0;
  }
}

@NgModule({
  providers: [],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatRadioModule,
    BrowserModule,
    MatExpansionModule,
    FormsModule
  ],
  declarations: [SettingsComponent],
  exports: [SettingsComponent]
})

export class SettingsModule {}


interface Model {
  modelObjectsStore: any;
  shapesStore: any;
  prototype: any;
}
