import { UnitInstance } from '../../models/UnitInstance';
import { Shape } from '../canvas/canvas.state';
import { ModelObjectsStateModel } from './model-objects.state';

export class AddUnitInstance {
  static readonly type = '[ModelObjects] add unitInstance';
  constructor(public unitInstance: UnitInstance) {}
}

export class UpdateUnitInstance {
  static readonly type = '[ModelObjects] update unitInstance';
  constructor(public unitInstanceToUpdate: UnitInstance) {}
}

export class RemoveUnitInstance {
  static readonly type = '[ModelObjects] remove unitInstance';
  constructor(public unitInstanceId: string) {}
}

export class AddLinkToNode {
  static readonly type = '[ModelObjects] add link';
  constructor(
    public nodeId: string,
    public inputShape: Shape,
    public outputShape: Shape
  ) {}
}

export class RemoveLinkToNode {
  static readonly type = '[ModelObjects] remove link';
  constructor(public inputShape: Shape, public outputShape: Shape) {}
}

export class AddModelParameterConfig {
  static readonly type = '[ModelObjects] add modelParameterConfig';
  constructor(
    public modelParameterConfig: {
      name: string;
      type: string;
      value: string | number | null;
    }
  ) {}
}

export class SetModelObjectsState {
  static readonly type = '[ModelObjects] set state';
  constructor(public newState?: ModelObjectsStateModel) {}
}
