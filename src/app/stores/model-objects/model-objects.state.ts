import {
  State,
  createSelector,
  Action,
  StateContext,
  Selector,
  Store
} from '@ngxs/store';
import { UnitInstance } from '../../models/UnitInstance';
import {
  AddUnitInstance,
  AddModelParameterConfig,
  RemoveUnitInstance,
  AddLinkToNode,
  RemoveLinkToNode,
  UpdateUnitInstance,
  SetModelObjectsState
} from './model-objects.actions';
import { ConfigService } from 'src/app/services/config/config.service';
import { Subtype } from 'src/app/models/Subtype';

const defaultState = {
  unitsInstance: [],
  modelParametersConfig: []
};

export interface ModelObjectsStateModel {
  unitsInstance: UnitInstance[];
  modelParametersConfig: {
    name: string;
    type: string;
    value: string | number | null;
  }[];
}

@State<ModelObjectsStateModel>({
  name: 'modelObjects',
  defaults: defaultState
})
export class ModelObjectsState {
  @Selector()
  static unitsInstance(state: ModelObjectsStateModel) {
    return state.unitsInstance;
  }

  @Selector()
  static modelParametersConfig(state: ModelObjectsStateModel) {
    return state.modelParametersConfig;
  }

  @Selector()
  static modelToSend(state: ModelObjectsStateModel) {
    return state;
  }

  static unitsInstanceById(unitInstanceId: string) {
    return createSelector(
      [ModelObjectsState],
      (state: ModelObjectsStateModel) => {
        let unitInstanceToReturn = state.unitsInstance.find(unitInstance => unitInstance.id === unitInstanceId);
        if (!unitInstanceToReturn) {
          state.unitsInstance.forEach(unitInstance => {
            if (unitInstance.children) {
              unitInstance.children.forEach(child => {
                if (child.id === unitInstanceId) {
                  unitInstanceToReturn = child;
                }
              });
            }
          });
        }
        return unitInstanceToReturn;
      }
    );
  }

  static unitsInstanceClassNameById(unitInstanceId: string) {
    return createSelector(
      [ModelObjectsState],
      (state: ModelObjectsStateModel) => {
        let unitInstanceToReturn = state.unitsInstance.find(unitInstance => unitInstance.id === unitInstanceId);
        if (!unitInstanceToReturn) {
          state.unitsInstance.forEach(unitInstance => {
            if (unitInstance.children) {
              unitInstance.children.forEach(child => {
                if (child.id === unitInstanceId) {
                  unitInstanceToReturn = child;
                }
              });
            }
          });
        }
        return unitInstanceToReturn.className;
      }
    );
  }

  constructor(
    private readonly configService: ConfigService,
    private readonly store: Store
  ) {}

  @Action(AddUnitInstance)
  addUnitInstance(
    ctx: StateContext<ModelObjectsStateModel>,
    { unitInstance }: AddUnitInstance
  ) {
    const state = ctx.getState();
    ctx.patchState({
      unitsInstance: [...state.unitsInstance, unitInstance]
    });
  }

  @Action(UpdateUnitInstance)
  updateUnitInstance(
    ctx: StateContext<ModelObjectsStateModel>,
    { unitInstanceToUpdate }: UpdateUnitInstance
  ) {
    const state = ctx.getState();
    let parentInstanceToUpdate;
    state.unitsInstance.forEach(unitInstance => {
      if (unitInstance.children) {
        unitInstance.children.forEach((child, index) => {
          if (child.id === unitInstanceToUpdate.id) {
            parentInstanceToUpdate = unitInstance;
            parentInstanceToUpdate.children[index] = unitInstanceToUpdate;
          }
        });
      }
    });
    ctx.patchState({
      unitsInstance: [
        ...state.unitsInstance.filter(
          unitInstance => unitInstance.id !== unitInstanceToUpdate.id
        ),
        unitInstanceToUpdate
      ]
    });
  }

  @Action(RemoveUnitInstance)
  removeUnitInstance(
    ctx: StateContext<ModelObjectsStateModel>,
    { unitInstanceId }: RemoveUnitInstance
  ) {
    const state = ctx.getState();
    const unitsInstanceToPatch = state.unitsInstance.filter(
      unitInstance => unitInstance.id !== unitInstanceId
    );

    ctx.patchState({
      unitsInstance: this.removeExportsToNodeAndLinksUnit(
        unitsInstanceToPatch,
        unitInstanceId
      )
    });
  }

  private removeExportsToNodeAndLinksUnit(
    unitsInstance: UnitInstance[],
    unitInstanceIdToRemove: string
  ): UnitInstance[] {
    return unitsInstance.map(unitInstance => {
      unitInstance.exportsToNode = unitInstance.exportsToNode.filter(
        exportToNode => !exportToNode.includes(unitInstanceIdToRemove)
      );
      unitInstance.linksUnits = unitInstance.linksUnits.filter(
        linkUnit => !linkUnit.includes(unitInstanceIdToRemove)
      );

      return unitInstance;
    });
  }

  @Action(AddLinkToNode)
  addLinkToNode(
    ctx: StateContext<ModelObjectsStateModel>,
    { nodeId, inputShape, outputShape }: AddLinkToNode
  ) {
    let linkToPush: string;
    let exportsToNodeToPush: string;
    const inputUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(inputShape.unitInstanceId)
    );
    const outputUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(outputShape.unitInstanceId)
    );

    const inputIsNode =
      this.configService.findUnitConfigByClassname(inputUnitInstanceClassname)
        .subtype === Subtype.NODE;
    const outputIsNode =
      this.configService.findUnitConfigByClassname(outputUnitInstanceClassname)
        .subtype === Subtype.NODE;
    const currentNodeIsInput = inputShape.unitInstanceId === nodeId;
    const currentNodeIsOutput = outputShape.unitInstanceId === nodeId;

    if (currentNodeIsOutput && !inputIsNode) {
      linkToPush = inputShape.unitInstanceId;
      if (this.configService.findUnitConfigByClassname(inputUnitInstanceClassname)
        .subtype === Subtype.REVERSIBLE) {
        linkToPush += '_in';
      }
    } else if (currentNodeIsInput && !outputIsNode) {
      linkToPush = outputShape.unitInstanceId;
      if (this.configService.findUnitConfigByClassname(outputUnitInstanceClassname)
        .subtype === Subtype.REVERSIBLE) {
        linkToPush += '_out';
      }
    } else if (currentNodeIsOutput && inputIsNode) {
      exportsToNodeToPush = inputShape.unitInstanceId;
    }

    const state = ctx.getState();
    const node = state.unitsInstance.find(
      unitInstance => unitInstance.id === nodeId
    );

    const linkExists =
      node.linksUnits.find(link => link === linkToPush) ||
      node.exportsToNode.find(
        exportToNode => exportToNode === exportsToNodeToPush
      );

    if (linkExists) {
      return;
    }

    if (linkToPush) {
      node.linksUnits.push(linkToPush);
    }
    if (exportsToNodeToPush) {
      node.exportsToNode.push(exportsToNodeToPush);
    }

    ctx.patchState({
      unitsInstance: [
        ...state.unitsInstance.filter(
          unitInstance => unitInstance.id !== nodeId
        ),
        node
      ]
    });
  }

  @Action(RemoveLinkToNode)
  removeLinkToNode(
    ctx: StateContext<ModelObjectsStateModel>,
    { inputShape, outputShape }: RemoveLinkToNode
  ) {
    const inputUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(inputShape.unitInstanceId)
    );
    const outputUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(outputShape.unitInstanceId)
    );
    const inputIsNode =
      this.configService.findUnitConfigByClassname(inputUnitInstanceClassname)
        .subtype === Subtype.NODE;
    const outputIsNode =
      this.configService.findUnitConfigByClassname(outputUnitInstanceClassname)
        .subtype === Subtype.NODE;

    const state = ctx.getState();
    let unitsInstanceToPatch: UnitInstance[];
    if (inputIsNode) {
      unitsInstanceToPatch = this.removeExportsToNodeAndLinksUnit(
        state.unitsInstance,
        outputShape.unitInstanceId
      );
    }

    if (outputIsNode) {
      unitsInstanceToPatch = this.removeExportsToNodeAndLinksUnit(
        state.unitsInstance,
        inputShape.unitInstanceId
      );
    }

    ctx.patchState({
      unitsInstance: unitsInstanceToPatch
    });
  }

  @Action(AddModelParameterConfig)
  addModelParameterConfig(
    ctx: StateContext<ModelObjectsStateModel>,
    { modelParameterConfig }: AddModelParameterConfig
  ) {
    const state = ctx.getState();
    ctx.patchState({
      modelParametersConfig: [
        ...state.modelParametersConfig.filter(
          parameter => parameter.name !== modelParameterConfig.name
        ),
        modelParameterConfig
      ]
    });
  }

  @Action(SetModelObjectsState)
  setModelObjectsState(
    ctx: StateContext<ModelObjectsStateModel>,
    { newState }: SetModelObjectsState
  ) {
    ctx.setState(newState ? newState : defaultState);
  }
}
