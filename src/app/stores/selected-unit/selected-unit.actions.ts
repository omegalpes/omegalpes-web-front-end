import { Shape } from '../canvas/canvas.state';

export class AddSelectedUnit {
  static readonly type = '[SelectedUnit] add selectedUnit';
  constructor(public shape: Shape) {}
}

export class RemoveSelectedUnit {
  static readonly type = '[SelectedUnit] remove selectedUnit';
}
