import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Shape } from '../canvas/canvas.state';
import { AddSelectedUnit, RemoveSelectedUnit } from './selected-unit.actions';

export interface SelectedUnitStateModel {
  shape: Shape;
}

@State<SelectedUnitStateModel>({
  name: 'selectedUnit',
  defaults: {
    shape: null
  }
})
export class SelectedUnitState {
  @Selector()
  static shape(state: SelectedUnitStateModel) {
    return state.shape;
  }

  constructor() {}

  @Action(AddSelectedUnit)
  addSelectedUnit(
    ctx: StateContext<SelectedUnitStateModel>,
    { shape }: AddSelectedUnit
  ) {
    ctx.setState({
      shape
    });
  }

  @Action(RemoveSelectedUnit)
  removeSelectedUnit(ctx: StateContext<SelectedUnitStateModel>) {
    ctx.setState({
      shape: null
    });
  }
}
