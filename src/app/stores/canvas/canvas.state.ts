import {
  State,
  createSelector,
  Action,
  StateContext,
  Selector,
  Store
} from '@ngxs/store';
import {
  AddShape,
  RemoveShape,
  MoveShape,
  AddLink,
  RemoveLink,
  ClearCanvas
} from './canvas.actions';
import SNAPSVG_TYPE from 'snapsvg';
import {
  RemoveUnitInstance,
  AddLinkToNode
} from '../model-objects/model-objects.actions';
import { Subtype } from 'src/app/models/Subtype';
import { ConfigService } from 'src/app/services/config/config.service';
import { ModelObjectsState } from '../model-objects/model-objects.state';

export interface CanvasStateModel {
  shapes: Shape[];
}

export interface Shape {
  id?: string;
  position: Position;
  connectors?: Connector[];
  svg?: SNAPSVG_TYPE.Element;
  unitInstanceId?: string;
  globalDirection?: Direction;
  isNode?: boolean;
  isChild?: boolean;
  children?: Shape[];
}

export interface Connector {
  links: Link[];
  direction: Direction;
  relativePosition: Position;
  svg?: SNAPSVG_TYPE.Element;
}

export interface Link {
  outputShapeId: string;
  inputShapeId: string;
  svg?: SNAPSVG_TYPE.Element;
}

export enum Direction {
  IN,
  OUT,
  BOTH
}

export interface Position {
  x: number;
  y: number;
}

const defaultState = {
  shapes: []
};
@State<CanvasStateModel>({
  name: 'canvas',
  defaults: defaultState
})
export class CanvasState {
  @Selector()
  static shapes(state: CanvasStateModel) {
    return state.shapes;
  }

  static shapeById(shapeId: string) {
    return createSelector([CanvasState], (state: CanvasStateModel) => {
      return state.shapes.find(shape => shape.id === shapeId);
    });
  }

  constructor(
    private readonly store: Store,
    private readonly configService: ConfigService
  ) {}

  @Action(AddShape)
  addShape(ctx: StateContext<CanvasStateModel>, { shape }: AddShape) {
    const state = ctx.getState();
    ctx.setState({
      shapes: [...state.shapes, shape]
    });
  }

  @Action(RemoveShape)
  removeShape(ctx: StateContext<CanvasStateModel>, { shapeId }: RemoveShape) {
    const state = ctx.getState();
    const shapeToRemove = state.shapes.find(shape => shape.id === shapeId);
    const shapesToUpdate = new Set<Shape>();

    // Removing all links on each linked shape to this one
    shapeToRemove.connectors.forEach(connector =>
      connector.links.forEach(link => {
        const inputShape = state.shapes.find(
          shape => shape.id === link.inputShapeId
        );
        const outputShape = state.shapes.find(
          shape => shape.id === link.outputShapeId
        );
        shapesToUpdate.add(inputShape);
        shapesToUpdate.add(outputShape);
        this.removeLinkInShape(link, inputShape);
        this.removeLinkInShape(link, outputShape);
      })
    );

    shapesToUpdate.forEach(shapeToUpdate => {
      ctx.setState({
        shapes: [
          ...state.shapes.filter(shape => shape !== shapeToUpdate),
          shapeToUpdate
        ]
      });
    });

    ctx.setState({
      shapes: [...state.shapes.filter(shape => shape.id !== shapeId)]
    });

    this.store.dispatch(new RemoveUnitInstance(shapeToRemove.unitInstanceId));
  }

  private removeLinkInShape(linkToFind: Link, shape: Shape) {
    shape.connectors.forEach(connector =>
      connector.links.forEach(linkConnector => {
        if (linkConnector === linkToFind) {
          connector.links = connector.links.filter(link => link !== linkToFind);
        }
      })
    );
  }

  @Action(MoveShape)
  moveShape(ctx: StateContext<CanvasStateModel>, { shapeId, x, y }: MoveShape) {
    const state = ctx.getState();
    const movedShaped = state.shapes.find(shape => shape.id === shapeId);
    movedShaped.position = { x, y };

    ctx.setState({
      shapes: [
        ...state.shapes.filter(shape => shape.id !== shapeId),
        movedShaped
      ]
    });
  }

  @Action(AddLink)
  addLink(ctx: StateContext<CanvasStateModel>, { link }: AddLink) {
    const state = ctx.getState();
    const outputShape = state.shapes.find(
      shape => shape.id === link.outputShapeId
    );
    const inputShape = state.shapes.find(
      shape => shape.id === link.inputShapeId
    );
    const inputUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(inputShape.unitInstanceId)
    );
    const outputUnitInstanceClassname = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(outputShape.unitInstanceId)
    );

    outputShape.connectors
      .find(connector => connector.direction === Direction.OUT)
      .links.push(link);
    inputShape.connectors
      .find(connector => connector.direction === Direction.IN)
      .links.push(link);

    ctx.setState({
      shapes: [
        ...state.shapes.filter(
          shape => shape !== outputShape && shape !== inputShape
        ),
        outputShape,
        inputShape
      ]
    });

    const inputIsNode =
      this.configService.findUnitConfigByClassname(inputUnitInstanceClassname)
        .subtype === Subtype.NODE;
    if (inputIsNode) {
      this.store.dispatch(
        new AddLinkToNode(inputShape.unitInstanceId, inputShape, outputShape)
      );
    }
    const outputIsNode =
      this.configService.findUnitConfigByClassname(outputUnitInstanceClassname)
        .subtype === Subtype.NODE;
    if (outputIsNode) {
      this.store.dispatch(
        new AddLinkToNode(outputShape.unitInstanceId, inputShape, outputShape)
      );
    }
  }

  @Action(RemoveLink)
  removeLink(ctx: StateContext<CanvasStateModel>, { link }: RemoveLink) {
    const state = ctx.getState();
    const outputShape = state.shapes.find(
      shape => shape.id === link.outputShapeId
    );
    const inputShape = state.shapes.find(
      shape => shape.id === link.inputShapeId
    );

    const outputConnector = outputShape.connectors.find(
      connector => connector.direction === Direction.OUT
    );
    const inputConnector = inputShape.connectors.find(
      connector => connector.direction === Direction.IN
    );

    outputConnector.links = outputConnector.links.filter(
      connectorLink => connectorLink !== link
    );
    inputConnector.links = inputConnector.links.filter(
      connectorLink => connectorLink !== link
    );

    ctx.setState({
      shapes: [
        ...state.shapes.filter(
          shape => shape !== outputShape && shape !== inputShape
        ),
        outputShape,
        inputShape
      ]
    });
  }

  @Action(ClearCanvas)
  clearCanvas(ctx: StateContext<CanvasStateModel>) {
    ctx.setState(defaultState);
  }
}
