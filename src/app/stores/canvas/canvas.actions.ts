import { Shape, Link } from './canvas.state';

export class AddShape {
  static readonly type = '[Canvas] add shape';
  constructor(public shape: Shape) {}
}

export class RemoveShape {
  static readonly type = '[Canvas] remove shape';
  constructor(public shapeId: string) {}
}

export class MoveShape {
  static readonly type = '[Canvas] move shape';
  constructor(public shapeId: string, public x: number, public y: number) {}
}

export class AddLink {
  static readonly type = '[Canvas] add link to shape';
  constructor(public link: Link) {}
}

export class RemoveLink {
  static readonly type = '[Canvas] remove link';
  constructor(public link: Link) {}
}

export class ClearCanvas {
  static readonly type = '[Canvas] clear';
}
