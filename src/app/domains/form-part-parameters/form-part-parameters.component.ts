import {Component, Input, NgModule, OnChanges} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UnitConfig, UnitParameterConfig} from 'src/app/models/Config';
import {
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatTooltipModule
} from '@angular/material';
import {ConfigService} from 'src/app/services/config/config.service';
import {Store} from '@ngxs/store';
import {ModelObjectsState} from 'src/app/stores/model-objects/model-objects.state';
import {UnitInstance} from 'src/app/models/UnitInstance';
import {Shape} from 'src/app/stores/canvas/canvas.state';
import {SelectedUnitState} from 'src/app/stores/selected-unit/selected-unit.state';
import {Subtype} from 'src/app/models/Subtype';

@Component({
  selector: 'app-form-part-parameters',
  templateUrl: './form-part-parameters.component.html',
  styleUrls: [
    './form-part-parameters.component.scss',
    '../properties-form/properties-form.component.scss'
  ]
})
export class FormPartParametersComponent implements OnChanges {
  @Input() shape: Shape;

  @Input() parametersGroup: FormGroup;
  @Input() parametersConfig: UnitParameterConfig[];
  @Input() parametersSingle: UnitParameterConfig[];

  unitConfig: UnitConfig;
  unitInstance: UnitInstance;
  selectedShape: Shape;

  constructor(
    private readonly configService: ConfigService,
    private readonly store: Store,
  ) {}

  ngOnChanges() {
    this.selectedShape = this.store.selectSnapshot(SelectedUnitState.shape);
    this.parametersSingle =  this.parametersConfig.filter(f => !f.label.includes('Energy type for '));
  }

  private isSingle(): boolean {
    const sh = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(this.selectedShape.unitInstanceId)
    );
    return this.configService.findUnitConfigByClassname(sh)
      .subtype === Subtype.SINGLE;
  }

}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatIconModule,
    MatTooltipModule
  ],
  declarations: [FormPartParametersComponent],
  exports: [FormPartParametersComponent]
})
export class FormPartParametersModule {}
