import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPartParametersComponent } from './form-part-parameters.component';

describe('FormPartParametersComponent', () => {
  let component: FormPartParametersComponent;
  let fixture: ComponentFixture<FormPartParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPartParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPartParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
