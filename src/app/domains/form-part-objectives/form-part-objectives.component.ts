import {Component, NgModule, Input, OnChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule, FormGroup, FormControl} from '@angular/forms';
import {
  MatInputModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatIconModule,
  MatTooltipModule,
  MatCheckboxModule
} from '@angular/material';
import {
  ObjectiveGroupConfig,
  DisjointObjectiveConfig, UnitObjectiveConfig
} from 'src/app/models/Config';
import {ModelObjectsState} from "../../stores/model-objects/model-objects.state";
import {Subtype} from "../../models/Subtype";
import {Shape} from "../../stores/canvas/canvas.state";
import {ConfigService} from "../../services/config/config.service";
import {Store} from "@ngxs/store";
import {SelectedUnitState} from "../../stores/selected-unit/selected-unit.state";

@Component({
  selector: 'app-form-part-objectives',
  templateUrl: './form-part-objectives.component.html',
  styleUrls: ['./form-part-objectives.component.scss']
})
export class FormPartObjectivesComponent implements OnChanges{
  @Input() objectivesGroup: FormGroup;
  @Input() objectiveGroupConfig: ObjectiveGroupConfig;
  selectedShape: Shape;
  absC : FormGroup
  absD : FormGroup

  constructor(
    private readonly configService: ConfigService,
    private readonly store: Store,
  ) {
    this.absC = new FormGroup({parameters: new FormControl() ,  active : new FormControl()});
    this.absD = new FormGroup({parameters: new FormControl() ,  active : new FormControl()});
  }

  public isActive(objectiveName: string): boolean {
    return (this.objectivesGroup.controls[objectiveName] as FormGroup)
      .controls['active'].value;
  }

  public checkboxClick(
    objectiveName: string,
    disjointObjectives: DisjointObjectiveConfig
  ) {
    disjointObjectives.objectives.forEach(disjointObjective => {
      if (disjointObjective.name !== objectiveName) {
        (this.objectivesGroup.controls[disjointObjective.name] as FormGroup)
          .controls['active'].setValue(false);
      }
    });
  }

  private isReversible(): boolean {
    const sh = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceClassNameById(this.selectedShape.unitInstanceId)
    );
    return this.configService.findUnitConfigByClassname(sh)
      .subtype === Subtype.REVERSIBLE;
  }

  ngOnChanges(): void {
    this.selectedShape = this.store.selectSnapshot(SelectedUnitState.shape);
    if (this.objectiveGroupConfig.objectives.length<2 && this.isReversible()) {
      this.objectivesGroup.addControl("minimize_production", this.absC)
      this.objectivesGroup.addControl("minimize_consumption", this.absD)
      let minprod: UnitObjectiveConfig = {
        name: "minimize_production", label: "minimize production", tooltip: "In order to minimize the production", parameters: []
      }
      let mincons: UnitObjectiveConfig = {
        name: "minimize_consumption", label: "minimize consumption", tooltip: "In order to minimize the consumption", parameters: []
      }
      this.objectiveGroupConfig.objectives.push(minprod)
      this.objectiveGroupConfig.objectives.push(mincons)
      this.objectiveGroupConfig.objectives=this.objectiveGroupConfig.objectives.filter(f=>!f.name.includes("minimize_exergy_destruction"))
      this.objectiveGroupConfig.type="Reversible"
    }

  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatIconModule,
    MatTooltipModule,
    MatCheckboxModule
  ],
  declarations: [FormPartObjectivesComponent],
  exports: [FormPartObjectivesComponent]
})
export class FormPartObjectivesModule {}
