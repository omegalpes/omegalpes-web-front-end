import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPartObjectivesComponent } from './form-part-objectives.component';

describe('FormPartObjectivesComponent', () => {
  let component: FormPartObjectivesComponent;
  let fixture: ComponentFixture<FormPartObjectivesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPartObjectivesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPartObjectivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
