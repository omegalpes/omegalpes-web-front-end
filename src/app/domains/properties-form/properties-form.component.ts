import {
  Component,
  NgModule,
  Input,
  OnChanges,
  HostListener
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Shape } from 'src/app/stores/canvas/canvas.state';
import { ConfigService } from 'src/app/services/config/config.service';
import {
  UnitConfig,
  EnergyConfig,
  UnitParameterConfig,
  ObjectiveGroupConfig,
  UnitConstraintConfig, UnitObjectiveConfig
} from 'src/app/models/Config';
import { Store } from '@ngxs/store';
import { ModelObjectsState } from 'src/app/stores/model-objects/model-objects.state';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  FormArray,
  Validators
} from '@angular/forms';
import {
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatExpansionModule,
  MatButtonModule,
  MatDialog,
  MatDialogModule,
  MatIconModule
} from '@angular/material';
import { UnitInstance } from 'src/app/models/UnitInstance';
import {
  DialogRemoveComponent,
  DialogRemoveModule
} from 'src/app/commons/dialog-remove/dialog-remove.component';
import { CanvasService } from 'src/app/services/canvas/canvas.service';
import { ToasterService } from 'src/app/services/toaster/toaster.service';
import { FormPartParametersModule } from '../form-part-parameters/form-part-parameters.component';
import { UnitInstancesService } from 'src/app/services/unit-instances/unit-instances.service';
import { UpdateUnitInstance } from 'src/app/stores/model-objects/model-objects.actions';
import { FormPartObjectivesModule } from '../form-part-objectives/form-part-objectives.component';
import { FormPartConstraintsModule } from '../form-part-constraints/form-part-constraints.component';
import { SelectedUnitState } from 'src/app/stores/selected-unit/selected-unit.state';
import { GraphicsService } from 'src/app/services/graphics/graphics.service';
import { IoService } from 'src/app/services/io/io.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {EnergyType} from 'src/app/models/EnergyType';

@Component({
  selector: 'app-properties-form',
  templateUrl: './properties-form.component.html',
  styleUrls: ['./properties-form.component.scss']
})
export class PropertiesFormComponent implements OnChanges {
  @Input() shape: Shape;

  unitConfig: UnitConfig;
  unitInstanceForm: FormGroup;
  selectedEnergy: EnergyConfig;
  prodEnergy: EnergyConfig;
  consEnergy: EnergyConfig;
  unitInstance: UnitInstance;
  unitParameters: UnitParameterConfig[];
  unitObjectives: ObjectiveGroupConfig[];
  name: FormControl;
  energyType: FormControl;
  parameters: FormArray;
  objectives: FormArray;
  disjointObjectives: FormArray;
  unitForm: FormGroup;
  isConversionUnit: boolean;
  isSingleUnit: boolean;
  nameProperties: string;
  power;
  co2;
  startCost;
  operatingCost;
  resultCSV: string;

  @HostListener('document:keydown.delete', ['$event']) onKeydownHandler(
    event: KeyboardEvent
  ) {
    const selectedShape = this.store.selectSnapshot(SelectedUnitState.shape);
    if (selectedShape.id === this.shape.id) {
      this.delete();
    }
  }

  constructor(
    private readonly configService: ConfigService,
    private readonly store: Store,
    private formBuilder: FormBuilder,
    private readonly dialog: MatDialog,
    private readonly toasterService: ToasterService,
    private readonly canvasService: CanvasService,
    private readonly unitInstanceService: UnitInstancesService,
    private readonly graphicService: GraphicsService,
    private readonly ioService: IoService
  ) {
    this.isConversionUnit = false;
    this.isSingleUnit = false;
  }

  ngOnChanges() {
    document.getElementById('unitName').focus();

    this.unitForm = this.formBuilder.group({
      name: ['', Validators.required],
      energy_type: ['', Validators.required],
      energy_type_prod: ['', Validators.required],
      energy_type_cons: ['', Validators.required],
      parameters: this.formBuilder.group({}),
      objectivesGroups: this.formBuilder.array([]),
      constraintsGroups: this.formBuilder.array([])
    });

    this.unitInstance = this.store.selectSnapshot(
      ModelObjectsState.unitsInstanceById(this.shape.unitInstanceId)
    );

    this.unitConfig = this.configService.findUnitConfigByClassname(
      this.unitInstance.className
    );
    this.isConversionUnit = this.isType(this.unitConfig, EnergyType.CONVERSION);
    this.isSingleUnit = this.isType(this.unitConfig, EnergyType.SINGLE);
    this.getSelectedEnergy();

    this.buildForm();
    this.fillFormFromUnitInstance();
  }

  private getFixedEnergy() {
    return this.unitInstance.className.startsWith('Fixed');
  }

  private getNode() {
    return this.unitInstance.className !== 'EnergyNode';
  }

  public async openFileCSVClick() {
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = '.csv, .txt';
    input.click();

    input.addEventListener('change', event => {
      if (!(event && event.target && event.target['files'])) {
        return;
      }
      const fileReader = new FileReader();
      fileReader.readAsText(event.target['files'][0]);
      const loadEvent = async e =>  {
        const csv: string = fileReader.result as string;
        let allTextLines = [];
        allTextLines = csv.split(/\r|\n|\r/);
        this.resultCSV = allTextLines[2].split(';');
        if (this.resultCSV.length > 0) {
          this.fillFormFromUnitInstance();
        }
        fileReader.removeEventListener('load', loadEvent);
      };
      fileReader.addEventListener('load', loadEvent);
    });
  }

  private getSelectedEnergy() {
    const unitInstanceEnergy = this.unitInstance.properties.find(
      property => property.name === 'energy_type'
    );
    const ProdEnergy = this.unitInstance.properties.find(
      property => property.name === 'energy_type_prod'
    );
    const ConsEnergy = this.unitInstance.properties.find(
      property => property.name === 'energy_type_cons'
    );
    const hasEnergyType = unitInstanceEnergy && unitInstanceEnergy.value;
    const hasProdType = ProdEnergy && ProdEnergy.value;
    const hasConsType = ConsEnergy && ConsEnergy.value;
    this.selectedEnergy = hasEnergyType
      ? this.unitConfig.energies.find(
          energy => energy.energy === unitInstanceEnergy.value
        )
      : this.unitConfig.energies[0];
    this.consEnergy = hasConsType
      ? this.unitConfig.energies.find(
          energy => energy.energy === ConsEnergy.value
        )
      : this.unitConfig.energies[0];
    this.prodEnergy = hasProdType
      ? this.unitConfig.energies.find(
          energy => energy.energy === ProdEnergy.value
        )
      : this.unitConfig.energies[0];
  }

  private isType(unitConfig: UnitConfig, energyType: EnergyType): boolean {
    return !!unitConfig.types.find(
      type => type.toLowerCase() === energyType.toLowerCase()
    );
  }

  public changeSelectedEnergy(energyName: string) {
    this.selectedEnergy = this.unitConfig.energies.find(
      energy => energy.energy === energyName
    );
    this.changeColor();
  }
  public changeConsSelectedEnergy(energyName: string) {
      this.consEnergy = this.unitConfig.energies.find(
        energy => energy.energy === energyName
      );
      this.changeConsColor();
  }
  public changeProdSelectedEnergy(energyName: string) {
        this.prodEnergy = this.unitConfig.energies.find(
          energy => energy.energy === energyName
        );
        this.changeProdColor();
    }

  private buildForm() {
    this.buildFormParameters();
    this.buildFormObjectives();
    this.buildFormConstraints();
  }

  private changeColor() {
    document.documentElement.style.setProperty(
      '--colorProperty',
      this.selectedEnergy.color
    );
    document.documentElement.style.setProperty(
      '--colorText',
      this.graphicService.getContrastColor(this.selectedEnergy.color)
    );
  }

  private changeProdColor() {
    document.documentElement.style.setProperty(
      '--colorPropertyProd',
      this.prodEnergy.color
    );
    document.documentElement.style.setProperty(
      '--colorTextProd',
      this.graphicService.getContrastColor(this.prodEnergy.color)
    );
  }

  private changeConsColor() {
    document.documentElement.style.setProperty(
      '--colorPropertyCons',
      this.consEnergy.color
    );
    document.documentElement.style.setProperty(
      '--colorTextCons',
      this.graphicService.getContrastColor(this.consEnergy.color)
    );
  }

  private fillFormFromUnitInstance() {
    // PARAMETERS
    const paramFormGroup = this.unitForm.get('parameters') as FormGroup;

    if (this.resultCSV == null ) {
      this.unitForm.controls['name'].setValue(
        this.unitInstanceService.getPropertyByName(this.unitInstance, 'name')
          .value
      );
    } else {
      this.unitForm.controls['name'].setValue(
      this.resultCSV[0]);
    }

    this.unitForm.controls['energy_type'].setValue(this.selectedEnergy.energy);
    this.unitForm.controls['energy_type_prod'].setValue(this.prodEnergy.energy);
    this.unitForm.controls['energy_type_cons'].setValue(this.consEnergy.energy);
    let counter = 1;
    this.unitInstance.properties
      .filter(property => property.name !== 'name'
          && property.name !== 'energy_type'
          && property.name !== 'energy_type_prod'
          && property.name !== 'energy_type_cons'
          && property.name !== 'energy_type_in'
          && property.name !== 'energy_type_out'
      )
      .forEach(property => {
        if (this.resultCSV == null) {
          paramFormGroup.get(property.name).setValue(property.value);
        } else {
          paramFormGroup.get(property.name).setValue(this.resultCSV[counter]);
        }
        counter++;
      });
    this.resultCSV = null;
    // OBJECTIVES
    const objectivesGroupsFormGroups = this.unitForm.get(
      'objectivesGroups'
    ) as FormArray;
    objectivesGroupsFormGroups.controls.forEach(
      (objectivesGroupsFormGroup: FormGroup) => {
        Object.keys(objectivesGroupsFormGroup.controls).forEach(
          objectiveName => {
            const objectiveFormGroup = objectivesGroupsFormGroup.controls[
              objectiveName
              ] as FormGroup;

            const currentObjective = this.unitInstance.objectives.find(
              objective => objective.name === objectiveName
            );
            objectiveFormGroup.controls['active'].setValue(currentObjective);
            if (currentObjective && objectiveFormGroup.value['parameters'] ) {
              // SET ALL OBJECTIVES PARAMETERS
              const paramaterFormGroup = objectiveFormGroup.controls[
                'parameters'
                ] as FormGroup;
              Object.keys(paramaterFormGroup.value).forEach(paramName => {

                const param = currentObjective.properties.find(
                  prop => prop.name === paramName
                );

                paramaterFormGroup.controls[paramName].setValue(
                  param ? param.value : undefined
                );
              });
            }
          }
        );
      }
    );

    // CONSTRAINTS
    const constraintsGroupsFormGroups = this.unitForm.get(
      'constraintsGroups'
    ) as FormArray;
    constraintsGroupsFormGroups.controls.forEach(
      (constraintGroupFormGroup: FormGroup) => {
        Object.keys(constraintGroupFormGroup.controls).forEach(
          constraintName => {
            const constraintFormGroup = constraintGroupFormGroup.controls[
              constraintName
            ] as FormGroup;

            const currentConstraint = this.unitInstance.constraints.find(
              constraint => constraint.name === constraintName
            );

            constraintFormGroup.controls['active'].setValue(currentConstraint);

            if (currentConstraint) {
              // SET ALL CONSTRAINTS PARAMETERS
              const paramaterFormGroup = constraintFormGroup.controls[
                'parameters'
              ] as FormGroup;
              Object.keys(paramaterFormGroup.value).forEach(paramName => {

                const param = currentConstraint.properties.find(
                  prop => prop.name === paramName
                );

                paramaterFormGroup.controls[paramName].setValue(
                  param ? param.value : undefined
                );
              });
            }
          }
        );
      }
    );
  }

  private buildFormParameters() {
    this.unitParameters = this.unitConfig.parameters.filter(
      parameter => parameter.name !== 'name'
        && parameter.name !== 'energy_type'
        && parameter.name !== 'energy_type_prod'
        && parameter.name !== 'energy_type_cons'
    );

    const paramFormGroup = this.unitForm.get('parameters') as FormGroup;

    this.unitParameters.forEach(parameter => {
      const validators = this.getValidators(parameter);
      paramFormGroup.addControl(
        parameter.name,
        new FormControl('', validators)
      );
    });
    this.changeColor();
    this.changeProdColor();
    this.changeConsColor();
  }

  private buildFormObjectives() {
    const objectivesForm = this.getFormGroupsObjectives();
    this.unitConfig.objectives_groups.forEach(objectivesGroup => {
      const objectivesFormGroup = this.formBuilder.group({});
      objectivesGroup.objectives.forEach(objective => {
        const objectiveFormGroup = this.formBuilder.group({});
        objectiveFormGroup.addControl('active', new FormControl(false));
        const parametersFormGroup = this.formBuilder.group({});
        objective.parameters.forEach(parameter => {
          const validators = this.getValidators(parameter);
          parametersFormGroup.addControl(
            parameter.name,
            new FormControl('', validators)
          );
        });
        objectiveFormGroup.addControl('parameters', parametersFormGroup);
        objectivesFormGroup.addControl(objective.name, objectiveFormGroup);
      });
      objectivesGroup.disjoint_objectives.forEach(disjoinctObjective => {
        disjoinctObjective.objectives.forEach( dObj => {
          const dObjFormGroup = this.formBuilder.group({});
          dObjFormGroup.addControl('active', new FormControl(false));
          objectivesFormGroup.addControl(dObj.name, dObjFormGroup);
        });
      });
      objectivesForm.push(objectivesFormGroup);
    });
  }

  private buildFormConstraints() {
    const constraintsForm = this.getFormGroupsConstraints();
    this.unitConfig.constraints_groups.forEach(constraintsGroup => {
      const constraintsFormGroup = this.formBuilder.group({});
      constraintsGroup.constraints.forEach(constraint => {
        const constraintFormGroup = this.formBuilder.group({});
        constraintFormGroup.addControl('active', new FormControl(false));

        const parametersFormGroup = this.formBuilder.group({});
        constraint.parameters.forEach(parameter => {
          const validators = this.getValidators(parameter);
          parametersFormGroup.addControl(
            parameter.name,
            new FormControl('', validators)
          );
        });
        constraintFormGroup.addControl('parameters', parametersFormGroup);

        constraintsFormGroup.addControl(
          constraint.constraint_name,
          constraintFormGroup
        );
      });
      constraintsForm.push(constraintsFormGroup);
    });
  }

  private getValidators(parameter: UnitParameterConfig) {
    const validators = [];
    if (parameter.required) {
      validators.push(Validators.required);
    }
    if (parameter.type === 'number') {
      validators.push(Validators.pattern(/\[-+]?[0-9]*(?:\.|,)[0-9]+|[0-9]+/));
    }
    return validators;
  }

  public getFormGroupParameters(): FormGroup {
    return this.unitForm.controls['parameters'] as FormGroup;
  }

  public getFormGroupsObjectives(): FormGroup[] {
    return (this.unitForm.controls['objectivesGroups'] as FormArray)
      .controls as FormGroup[];
  }

  public getFormGroupsConstraints(): FormGroup[] {
    return (this.unitForm.controls['constraintsGroups'] as FormArray)
      .controls as FormGroup[];
  }
  private isChild(): boolean {
    return !this.store.selectSnapshot(SelectedUnitState.shape).isChild;
  }
  private isReversible(): boolean {
    return this.unitConfig.label === 'Reversible Unit';
  }

  private isReversibleConversion(): boolean {
    return this.unitConfig.label === 'Reversible Conversion Unit';
  }

  private setProperty(propertyName: string, value: string | number) {
    this.unitInstance.properties.push({
      name: propertyName,
      value,
      type: this.unitConfig.parameters.find(
        param => param.name === propertyName
    ).type
    });
  }
  private getObjectivePropertyType(
    objectiveName: string,
    parameterName: string
  ) {
    let objectiveFound: UnitObjectiveConfig;

    this.unitConfig.objectives_groups.forEach(objectivesGroup => {
      objectivesGroup.objectives.forEach(objective => {
        if (objective.name === objectiveName) {
          objectiveFound = objective;
        }
      });
    });

    return objectiveFound.parameters.find(
      param => param.name === parameterName
    ).type;
  }
  private getConstraintPropertyType(
    constraintName: string,
    parameterName: string
  ) {
    let constraintFound: UnitConstraintConfig;

    this.unitConfig.constraints_groups.forEach(constraintsGroup => {
      constraintsGroup.constraints.forEach(constraint => {
        if (constraint.constraint_name === constraintName) {
          constraintFound = constraint;
        }
      });
    });

    return constraintFound.parameters.find(
      param => param.name === parameterName
    ).type;
  }

  public save() {
    this.saveProperties();
    this.saveObjectives();
    this.saveConstraints();

    const unitInstanceToUpdate = {
      ...this.unitInstance
    };
    this.store.dispatch(new UpdateUnitInstance(unitInstanceToUpdate));
    this.toasterService.openToast(`Unit saved.`, 'Ok');
  }

  private saveProperties() {
    this.unitInstance.properties = [];
    this.setProperty('name', this.unitForm.controls['name'].value);
    if (this.isReversible()) {
      this.setProperty(
        'energy_type_cons',
        this.unitForm.controls['energy_type_cons'].value
      );
      this.setProperty(
        'energy_type_prod',
        this.unitForm.controls['energy_type_prod'].value
      );
   }
    if (this.isSingleUnit) {
      this.setProperty(
        'energy_type_in',
        this.unitInstance.children[0].properties.find(p => p.name === 'energy_type').value
      );
      this.setProperty(
        'energy_type_out',
        this.unitInstance.children[1].properties.find(p => p.name === 'energy_type').value
      );
    }
    if (!this.isConversionUnit && !this.isReversible() && !this.isSingleUnit && !this.isReversibleConversion()) {
      this.setProperty(
        'energy_type',
        this.unitForm.controls['energy_type'].value
      );
    }
    const groupParametersValue = this.getFormGroupParameters().value;
    Object.keys(groupParametersValue)
      .filter(key => groupParametersValue[key])
      .forEach(key => {
        this.setProperty(key, groupParametersValue[key]);
      });
  }

  private saveObjectives() {
    this.unitInstance.objectives = [];

    this.getFormGroupsObjectives().forEach(groupObjectives => {
      Object.keys(groupObjectives.value)
        .filter(
          key =>
            groupObjectives.value[key] && groupObjectives.value[key]['active']

    )
        .forEach(key => {
          if (groupObjectives.value[key]['parameters']) {

            const properties = Object.keys(
              groupObjectives.value[key]['parameters']
            )
              .filter(
                parameterKey =>
                  groupObjectives.value[key]['parameters'][parameterKey]
              )
              .map(parameterKey => ({
                name: parameterKey,
                type: this.getObjectivePropertyType(key, parameterKey),
                value: groupObjectives.value[key]['parameters'][parameterKey]
              }));

            this.unitInstance.objectives.push({
              name: key,
              properties
            });
          } else {
            this.unitInstance.objectives.push({
              name: key, properties: []
            });
          }
        });
    });
  }

  private saveConstraints() {
    this.unitInstance.constraints = [];

    this.getFormGroupsConstraints().forEach(groupConstraints => {
      Object.keys(groupConstraints.value)
        .filter(
          key =>
            groupConstraints.value[key] && groupConstraints.value[key]['active']
        )
        .forEach(key => {
          const properties = Object.keys(
            groupConstraints.value[key]['parameters']
          )
            .filter(
              parameterKey =>
                groupConstraints.value[key]['parameters'][parameterKey]
            )
            .map(parameterKey => ({
              name: parameterKey,
              type: this.getConstraintPropertyType(key, parameterKey),
              value: groupConstraints.value[key]['parameters'][parameterKey]
            }));

          this.unitInstance.constraints.push({
            name: key,
            properties
          });
        });
    });
  }

  public delete(): void {
    const dialogRef = this.dialog.open(DialogRemoveComponent, {
      width: '16rem',
      data: `Would you really delete ${
        this.unitInstanceService.getPropertyByName(this.unitInstance, 'name')
          .value
      } ?`
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.canvasService.removeShape(this.shape);
      }
    });
  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatExpansionModule,
    MatButtonModule,
    DialogRemoveModule,
    MatDialogModule,
    FormPartParametersModule,
    FormPartObjectivesModule,
    FormPartConstraintsModule,
    MatIconModule,
  ],
  declarations: [PropertiesFormComponent],
  exports: [PropertiesFormComponent]
})
export class PropertiesFormModule {}
