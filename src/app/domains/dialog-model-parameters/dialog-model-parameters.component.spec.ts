import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModelParametersComponent } from './dialog-model-parameters.component';

describe('DialogModelParametersComponent', () => {
  let component: DialogModelParametersComponent;
  let fixture: ComponentFixture<DialogModelParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogModelParametersComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogModelParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
