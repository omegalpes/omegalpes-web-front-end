import { Component, NgModule, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModelParameterConfig } from 'src/app/models/Config';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-dialog-model-parameters',
  templateUrl: './dialog-model-parameters.component.html',
  styleUrls: ['./dialog-model-parameters.component.scss']
})
export class DialogModelParametersComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogModelParametersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModelParameterConfig[]
  ) {
    data.forEach(modelParameterConfig => {
      modelParameterConfig.value = modelParameterConfig.value
        ? modelParameterConfig.value
        : modelParameterConfig.default;
    });
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  onsaveClick(): void {
    this.dialogRef.close(true);
  }
}

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule
  ],
  entryComponents: [DialogModelParametersComponent],
  declarations: [DialogModelParametersComponent],
  exports: [DialogModelParametersComponent]
})
export class DialogModelParametersModule {}
