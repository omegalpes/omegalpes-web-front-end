import { Component, NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import {
  MatInputModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatIconModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatCardModule
} from '@angular/material';
import { ConstraintGroupConfig } from 'src/app/models/Config';

@Component({
  selector: 'app-form-part-constraints',
  templateUrl: './form-part-constraints.component.html',
  styleUrls: ['./form-part-constraints.component.scss']
})
export class FormPartConstraintsComponent {
  @Input() constraintsGroup: FormGroup;
  @Input() constraintsGroupConfig: ConstraintGroupConfig;

  public isActive(constraintName: string): boolean {
    return (this.constraintsGroup.controls[constraintName] as FormGroup)
      .controls['active'].value;
  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatIconModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatCardModule
  ],
  declarations: [FormPartConstraintsComponent],
  exports: [FormPartConstraintsComponent]
})
export class FormPartConstraintsModule {}
