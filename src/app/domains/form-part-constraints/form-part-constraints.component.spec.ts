import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPartConstraintsComponent } from './form-part-constraints.component';

describe('FormPartConstraintsComponent', () => {
  let component: FormPartConstraintsComponent;
  let fixture: ComponentFixture<FormPartConstraintsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPartConstraintsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPartConstraintsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
